from django import conf
from storages.backends import s3boto3


class MediaStorage(s3boto3.S3Boto3Storage):
    location = conf.settings.MEDIA_LOCATION


class StaticStorage(s3boto3.S3Boto3Storage):
    location = conf.settings.STATIC_LOCATION
