from django import conf, urls
from rest_framework import documentation

urlpatterns = [
    urls.path('', urls.include('api.urls')),
]

if conf.settings.DEBUG:
    urlpatterns += [
        urls.path('docs/', documentation.include_docs_urls(title='Home Industry API')),
    ]
