import boto3
from django import conf
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions as rest_framework_exceptions

from api import models


def get_app_config():
    try:
        app_config = models.AppConfig.objects.get()
    except models.AppConfig.DoesNotExist:
        raise rest_framework_exceptions.APIException(_('A bad configuration error occurred.'))
    return app_config


def get_aws_settings():
    try:
        aws_settings = conf.settings.AWS
    except AttributeError:
        raise rest_framework_exceptions.APIException(_('A bad configuration error occurred.'))
    return aws_settings


def get_shipment_config():
    try:
        shipment_config = models.ShipmentConfig.objects.get()
    except models.ShipmentConfig.DoesNotExist:
        raise rest_framework_exceptions.APIException(_('A bad configuration error occurred.'))
    return shipment_config


def send_sms(phone_number, message):
    aws_settings = get_aws_settings()
    app_config = get_app_config()
    if not app_config.send_sms:
        raise rest_framework_exceptions.APIException(_('Server is currently unable to send SMS.'))
    client = boto3.client(
        'sns',
        aws_access_key_id=aws_settings['AWS_ACCESS_KEY_ID'],
        aws_secret_access_key=aws_settings['AWS_SECRET_ACCESS_KEY'],
        region_name=aws_settings['AWS_REGION_NAME']
    )
    client.publish(PhoneNumber=phone_number, Message=message)
