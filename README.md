# Home Industry API

[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/sosial/pilar/pilar-backend/badges/dev/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/sosial/pilar/pilar-backend/-/commits/dev)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/sosial/pilar/pilar-backend/badges/dev/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/sosial/pilar/pilar-backend/-/commits/dev)
## Table of Contents

- [Environment Variables](#environment-variables)
  - [Local](#local)
  - [CI](#ci)
  - [Staging](#staging)
  - [Production](#production)
- [Local Configuration](#local-configuration)
- [Deployed API URLs](#deployed-api-urls)
- [API Documentation](#api-documentation)

## Environment Variables

### Local

Key | Required | Example
--- | --- | ---
`DATABASE_HOST` | yes | `127.0.0.1`
`DATABASE_NAME` | yes | `home_industry`
`DATABASE_PASSWORD` | yes | `postgres`
`DATABASE_PORT` | yes | `5432`
`DATABASE_USER` | yes | `postgres`
`DEBUG` | no | `True`
`DJANGO_SETTINGS_MODULE` | yes | `home_industry.settings.local`
`SECRET_KEY` | yes | `7&s33ax$lxxzti1)0y=8#tu!$7bdy)p$1@kn06tp&8x8i9#h2u`

### CI

Key | Required | Example
--- | --- | ---
`DJANGO_SETTINGS_MODULE` | yes | `home_industry.settings.ci`
`SECRET_KEY` | yes | `7&s33ax$lxxzti1)0y=8#tu!$7bdy)p$1@kn06tp&8x8i9#h2u`

### Staging

Key | Required | Example
--- | --- | ---
`AWS_ACCESS_KEY_ID` | yes | `AKIAIOSFODNN7EXAMPLE`
`AWS_REGION_NAME` | yes | `ap-southeast-1`
`AWS_SECRET_ACCESS_KEY` | yes | `wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY`
`AWS_STORAGE_BUCKET_NAME` | yes | `homeindustry-api`
`DATABASE_URL` | yes | `postgres://postgres:postgres@ec2-117-21-174-214.compute-1.amazonaws.com:5432/home_industry`
`DEBUG` | no | `True`
`DJANGO_SETTINGS_MODULE` | yes | `home_industry.settings.staging`
`HOME_INDUSTRY_ADMIN_SITE_URL` | no | `https://homeindustry.com/admin/`
`HOME_INDUSTRY_ADMIN_SITE_USER_PATH` | no | `users/`
`HOME_INDUSTRY_ADMIN_SITE_PRODUCT_PATH` | no | `products/`
`HOME_INDUSTRY_ADMIN_SITE_TRANSACTION_PATH` | no | `transactions/`
`HOME_INDUSTRY_ADMIN_SITE_PROGRAM_PATH` | no | `programs/`
`HOME_INDUSTRY_ADMIN_SITE_PROGRAM_DONATION_PATH` | no | `program-donations/`
`SECRET_KEY` | yes | `7&s33ax$lxxzti1)0y=8#tu!$7bdy)p$1@kn06tp&8x8i9#h2u`

### Production

Key | Required | Example
--- | --- | ---
`AWS_ACCESS_KEY_ID` | yes | `AKIAIOSFODNN7EXAMPLE`
`AWS_REGION_NAME` | yes | `ap-southeast-1`
`AWS_SECRET_ACCESS_KEY` | yes | `wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY`
`AWS_STORAGE_BUCKET_NAME` | yes | `homeindustry-api`
`DATABASE_HOST` | yes | `127.0.0.1`
`DATABASE_NAME` | yes | `home_industry`
`DATABASE_PASSWORD` | yes | `postgres`
`DATABASE_PORT` | yes | `5432`
`DATABASE_USER` | yes | `postgres`
`DJANGO_SETTINGS_MODULE` | yes | `home_industry.settings.production`
`HOME_INDUSTRY_ADMIN_SITE_URL` | no | `https://homeindustry.com/admin/`
`HOME_INDUSTRY_ADMIN_SITE_USER_PATH` | no | `users/`
`HOME_INDUSTRY_ADMIN_SITE_PRODUCT_PATH` | no | `products/`
`HOME_INDUSTRY_ADMIN_SITE_TRANSACTION_PATH` | no | `transactions/`
`HOME_INDUSTRY_ADMIN_SITE_PROGRAM_PATH` | no | `programs/`
`HOME_INDUSTRY_ADMIN_SITE_PROGRAM_DONATION_PATH` | no | `program-donations/`
`SECRET_KEY` | yes | `7&s33ax$lxxzti1)0y=8#tu!$7bdy)p$1@kn06tp&8x8i9#h2u`

## Local Configuration

- Create environment variables in root folder (change as needed)

  Filename: `.env`
  ```bash
  DATABASE_HOST=db
  DATABASE_NAME=home_industry
  DATABASE_PASSWORD=postgres
  DATABASE_PORT=5432
  DATABASE_USER=postgres
  DJANGO_SETTINGS_MODULE=home_industry.settings.local
  SECRET_KEY=7&s33ax$lxxzti1)0y=8#tu!$7bdy)p$1@kn06tp&8x8i9#h2u
  ALLOWED_HOST=0.0.0.0
  ```
- Change `wait-for-it.sh` permission
  ```bash
  $ chmod +x wait-for-it.sh
  ```
- Run docker-compose
  ```bash
  $ sudo docker-compose up
  ```
- In another terminal, check running container
  ```bash
  $ docker ps
  ```
- Run collectstatic
  ```bash
  $ docker exec -it <web-container-id> python manage.py collectstatic --noinput
  ```
- Create superuser
  ```bash
  $ docker exec -it <web-container-id> python manage.py createsuperuser
  ```
- Create or update API configuration in database
  ```bash
  $ docker exec -it <web-container-id> python manage.py createorupdateapiconfig
  ```
- Generate dummy data from seeders for database
  ```bash
  $ docker exec -it <web-container-id> python manage.py createdummydata
  ```
- Access database
  ```bash
  $ docker exec -it <postgres-container-id> psql -U postgres -d home_industry -h db
  ```

## Deployed API URLs

- Development: [https://pilar-be-dev.cs.ui.ac.id](https://pilar-be-dev.cs.ui.ac.id)
- Staging: [https://pilar-be-staging.cs.ui.ac.id](https://pilar-be-staging.cs.ui.ac.id)
- Production: [https://pilar-be.cs.ui.ac.id](https://pilar-be.cs.ui.ac.id)

## API Documentation

[https://pilar-be-dev.cs.ui.ac.id/docs/](https://pilar-be-dev.cs.ui.ac.id/docs/)
