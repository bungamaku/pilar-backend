from rest_framework import exceptions, status


class IntegrityError(exceptions.APIException):
    status_code = status.HTTP_409_CONFLICT
