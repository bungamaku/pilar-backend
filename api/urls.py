from django import urls
from knox import views as knox_views

from api import views as api_views

urlpatterns = [
    urls.path('auth/register/', api_views.AuthRegister.as_view(), name='auth-register'),
    urls.path('auth/cred-login/', api_views.AuthCredLogin.as_view(), name='auth-cred-login'),
    urls.path(
        'auth/phone-number-login/',
        api_views.AuthPhoneNumberLogin.as_view(),
        name='auth-phone-number-login'
    ),
    urls.path('auth/otp-login/', api_views.AuthOTPLogin.as_view(), name='auth-otp-login'),
    urls.path('auth/resend-otp/', api_views.AuthResendOTP.as_view(), name='auth-resend-otp'),
    urls.path('auth/logout/', knox_views.LogoutView.as_view(), name='auth-logout'),
    urls.path('auth/logout-all/', knox_views.LogoutAllView.as_view(), name='auth-logout-all'),
    urls.path('cart/update/', api_views.CartUpdate.as_view(), name='cart-update'),
    urls.path('cart/overview/', api_views.CartOverview.as_view(), name='cart-overview'),
    urls.path('cart/checkout/', api_views.CartCheckout.as_view(), name='cart-checkout'),
    urls.path('cart/upload-pop/', api_views.CartUploadPOP.as_view(), name='cart-upload-pop'),
    urls.path(
        'cart/complete-transaction/',
        api_views.CartCompleteTransaction.as_view(),
        name='cart-complete-transaction'
    ),
    urls.path(
        'cart/cancel-transaction/',
        api_views.CartCancelTransaction.as_view(),
        name='cart-cancel-transaction'
    ),
    urls.path('donation/create/', api_views.DonationCreate.as_view(), name='donation-create'),
    urls.path('donation/delete-by-p/<str:pid>', api_views.delete_donation_by_program, name='donation-by-program'),
    urls.path(
        'donation/reupload-proof-of-bank-transfer/',
        api_views.DonationReuploadProofOfBankTransfer.as_view(),
        name='donation-reupload-proof-of-bank-transfer'
    ),
    urls.path(
        'reports/transaction/',
        api_views.ReportTransaction.as_view(),
        name='transaction-report'
        ),
    urls.path(
        'reports/program-donation/csh',
        api_views.ReportProgramDonationCSH.as_view(),
        name='program-donation-report-csh'
    ),
    urls.path(
        'reports/program-donation/gds',
        api_views.ReportProgramDonationGDS.as_view(),
        name='program-donation-report-gds'
    ),

    urls.path('users/', api_views.UserList.as_view(), name='user-list'),
    urls.path('users/<str:pk>/', api_views.UserDetail.as_view(), name='user-detail'),
    urls.path(
        'bank-account-transfer-destinations/',
        api_views.BankAccountTransferDestinationList.as_view(),
        name='bank-account-transfer-destination-list'
    ),
    urls.path(
        'bank-account-transfer-destinations/<str:pk>/',
        api_views.BankAccountTransferDestinationDetail.as_view(),
        name='bank-account-transfer-destination-detail'
    ),
    urls.path('categories/', api_views.CategoryList.as_view(), name='category-list'),
    urls.path('categories/<str:pk>/', api_views.CategoryDetail.as_view(), name='category-detail'),
    urls.path('subcategories/', api_views.SubcategoryList.as_view(), name='subcategory-list'),
    urls.path(
        'subcategories/<str:pk>/',
        api_views.SubcategoryDetail.as_view(),
        name='subcategory-detail'
    ),
    urls.path('products/', api_views.ProductList.as_view(), name='product-list'),
    urls.path('products/<str:pk>/', api_views.ProductDetail.as_view(), name='product-detail'),
    urls.path('shopping-carts/', api_views.ShoppingCartList.as_view(), name='shopping-cart-list'),
    urls.path(
        'shopping-carts/<str:pk>/',
        api_views.ShoppingCartDetail.as_view(),
        name='shopping-cart-detail'
    ),
    urls.path('transactions/', api_views.TransactionList.as_view(), name='transaction-list'),
    urls.path(
        'transactions/<str:pk>/',
        api_views.TransactionDetail.as_view(),
        name='transaction-detail'
    ),
    urls.path('programs/', api_views.ProgramList.as_view(), name='program-list'),
    urls.path('programs/<str:pk>/', api_views.ProgramDetail.as_view(), name='program-detail'),
    urls.path(
        'programs/<str:program>/progress',
        api_views.ProgramProgressList.as_view(),
        name='program-progress-list'
    ),
    urls.path(
        'program-donations/csh',
        api_views.ProgramDonationListCSH.as_view(),
        name='program-donation-csh-list'
    ),
    urls.path(
        'program-donations/gds',
        api_views.ProgramDonationListGDS.as_view(),
        name='program-donation-gds-list'
    ),
    urls.path(
        'program-donations/',
        api_views.ProgramDonationList.as_view(),
        name='program-donation-list'
    ),
    urls.path(
        'program-donations/<str:pk>/',
        api_views.ProgramDonationDetail.as_view(),
        name='program-donation-detail'
    ),
    urls.path(
        'choices/payment-method/',
        api_views.PaymentMethodChoices.as_view(),
        name='payment-method-choice'
    ),
    urls.path(
        'choices/transaction-status/',
        api_views.TransactionStatusChoices.as_view(),
        name='transaction-status-choice'
    ),
    urls.path(
        'choices/donation-status/',
        api_views.DonationStatusChoices.as_view(),
        name='donation-status-choice'
    ),
    urls.path('configs/app/', api_views.AppConfigDetail.as_view(), name='app-config-detail'),
    urls.path(
        'configs/help-contact/',
        api_views.HelpContactConfigDetail.as_view(),
        name='help-contact-config-detail'
    ),
    urls.path(
        'configs/shipment/',
        api_views.ShipmentConfigDetail.as_view(),
        name='shipment-config-detail'
    ),
    urls.path('batch/', api_views.BatchList.as_view(), name='batch-list'),
    urls.path('batch/<str:pk>/', api_views.BatchDetail.as_view(), name='batch-detail'),
    urls.path('batch/create/', api_views.BatchCreate.as_view(), name='batch-create'),
]
