from django import dispatch
from django.db.models import signals

from api import models, utils


@dispatch.receiver(signals.post_save, sender=models.User)
def create_shopping_cart(sender, created, instance, **_kwargs): # pylint: disable=unused-argument
    if created:
        models.ShoppingCart.objects.create(user=instance)


@dispatch.receiver(signals.pre_save, sender=models.Transaction)
def fill_dependent_transaction_fields(sender, instance, **_kwargs):
    try:
        obj = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        obj = None
    if (obj is None) or (obj.user != instance.user) or (getattr(instance, 'update_user', False)):
        if instance.user is None:
            instance.user_full_name = None
            instance.user_phone_number = None
            instance.shipping_address = None
            instance.shipping_neighborhood = None
            instance.shipping_hamlet = None
            instance.shipping_urban_village = None
            instance.shipping_sub_district = None
            instance.shipping_costs = None
        else:
            instance.user_full_name = instance.user.full_name
            instance.user_phone_number = instance.user.phone_number
            instance.shipping_address = instance.user.address
            instance.shipping_neighborhood = instance.user.neighborhood
            instance.shipping_hamlet = instance.user.hamlet
            instance.shipping_urban_village = instance.user.urban_village
            instance.shipping_sub_district = instance.user.sub_district
            instance.shipping_costs = utils.get_shipping_costs(instance.user)
    if ((obj is None) or
            (obj.bank_account_transfer_destination != instance.bank_account_transfer_destination)
            or (getattr(instance, 'update_bank_account_transfer_destination', False))):
        if instance.bank_account_transfer_destination is None:
            instance.transfer_destination_bank_name = None
            instance.transfer_destination_bank_account_name = None
            instance.transfer_destination_bank_code_number = None
            instance.transfer_destination_bank_account_number = None
        else:
            instance.transfer_destination_bank_name = (
                instance.bank_account_transfer_destination.bank_name
            )
            instance.transfer_destination_bank_account_name = (
                instance.bank_account_transfer_destination.bank_account_name
            )
            instance.transfer_destination_bank_account_number = (
                instance.bank_account_transfer_destination.bank_account_number
            )
            instance.transfer_destination_bank_code_number = (
                instance.bank_account_transfer_destination.bank_code_number
            )
    
    if (instance.transaction_status == '002'):
        if instance.batch is None:
            instance.batch = utils.get_batch_transaction(instance)
        instance.batch_name = instance.batch.batch_name
        instance.end_date = instance.batch.end_date


@dispatch.receiver(signals.pre_save, sender=models.TransactionItem)
def fill_dependent_transaction_item_fields(sender, instance, **_kwargs):
    try:
        obj = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        obj = None
    if ((obj is None) or
            (obj.product != instance.product) or
            (getattr(instance, 'update_product', False))):
        if instance.product is None:
            instance.product_name = None
            instance.product_price = None
            instance.hampers_price = None
            instance.hampers_messages = None
            
        else:
            instance.product_name = instance.product.name
            instance.product_price = instance.product.price
            instance.hampers_price = instance.hampers_price
            instance.hampers_messages = instance.hampers_messages
            


@dispatch.receiver(signals.pre_save, sender=models.ProgramDonation)
def fill_dependent_program_donation_fields(sender, instance, **_kwargs):
    try:
        obj = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        obj = None
    if ((obj is None) or
            (obj.program != instance.program) or
            (getattr(instance, 'update_program', False))):
        if instance.program is None:
            instance.program_name = None
        else:
            instance.program_name = instance.program.name
    if ((obj is None) or
            (obj.user != instance.user) or
            (getattr(instance, 'update_user', False))):
        if instance.user is None:
            instance.user_full_name = None
            instance.user_phone_number = None
        else:
            instance.user_full_name = instance.user.full_name
            instance.user_phone_number = instance.user.phone_number

    if (obj is not None) and (instance.donation_type != None):
        if instance.donation_type == 'CSH':
            if instance.bank_account_transfer_destination is None:
                instance.bank_account_transfer_destination = utils.get_transfer_destination(instance)
            instance.transfer_destination_bank_name = (
                instance.bank_account_transfer_destination.bank_name
            )
            instance.transfer_destination_bank_account_name = (
                instance.bank_account_transfer_destination.bank_account_name
            )
            instance.transfer_destination_bank_account_number = (
                instance.bank_account_transfer_destination.bank_account_number
            )



            