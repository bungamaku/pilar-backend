from api import filters as api_filters
from api import models, paginations
from api import permissions as api_permissions
from api import schemas
from api import serializers as api_serializers
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django_filters import rest_framework
from rest_framework import exceptions as rest_framework_exceptions
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework.decorators import api_view


class ProgramProgressList(generics.ListCreateAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    lookup_field = 'program'
    serializer_class = api_serializers.ProgramProgressSerializer

    def get_queryset(self):
        return models.ProgramProgress.objects.filter(program=self.get_program())

    def perform_create(self, serializer):
        serializer.save(program=self.get_program())

    def get_program(self):
        program = models.Program.objects.filter(
            id=self.kwargs.get(self.lookup_field)).first()
        if program is None:
            raise rest_framework_exceptions.NotFound(_(
                'Program not found.'
            ))
        return program


class ProgramList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_fields = ['code']
    ordering_fields = ['name', 'start_date_time', 'end_date_time']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Program.objects.all()
    search_fields = ['code', 'name']
    serializer_class = api_serializers.ProgramSerializer


class ProgramDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Program.objects.all()
    serializer_class = api_serializers.ProgramSerializer


class ProgramDonationList(generics.ListAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_class = api_filters.ProgramDonationFilter
    ordering_fields = ['created_at', 'updated_at']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    queryset = models.ProgramDonation.objects.all()
    schema = schemas.ProgramDonationListSchema()
    search_fields = ['donation_number', 'user_full_name',
                     'program_name', 'donation_type']
    serializer_class = api_serializers.ProgramDonationSerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if not self.request.user.is_staff:
            return queryset.filter(user=self.request.user)
        return queryset


@api_view(['DELETE'])
@csrf_exempt
def delete_donation_by_program(request, pid):
    if ((request.user) and (request.user.is_staff)):
        program = models.Program.objects.get(id=pid)
        donation = models.ProgramDonation.objects.filter(program=program)
        donation.delete()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class ProgramDonationListCSH(generics.ListAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_class = api_filters.ProgramDonationFilter
    ordering_fields = ['created_at', 'updated_at']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    queryset = models.ProgramDonation.objects.filter(donation_type='CSH')
    schema = schemas.ProgramDonationListSchema()
    search_fields = ['donation_number', 'user_full_name',
                     'program_name', 'donation_type']
    serializer_class = api_serializers.ProgramDonationSerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if not self.request.user.is_staff:
            return queryset.filter(user=self.request.user)
        return queryset


class ProgramDonationListGDS(generics.ListAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_class = api_filters.ProgramDonationFilter
    ordering_fields = ['created_at', 'updated_at']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    queryset = models.ProgramDonation.objects.filter(donation_type='GDS')
    schema = schemas.ProgramDonationListSchema()
    search_fields = ['donation_number', 'user_full_name',
                     'program_name', 'donation_type']
    serializer_class = api_serializers.ProgramDonationSerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        # if not self.request.user.is_staff:
        #     return queryset.filter(user=self.request.user)
        # return queryset
        return queryset.filter(user=self.request.user)


class ProgramDonationDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrOwnerReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.ProgramDonation.objects.all()
    serializer_class = api_serializers.ProgramDonationSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if instance.donation_status in ('002', '003'):
            raise rest_framework_exceptions.PermissionDenied(_(
                'Cannot update program donation because it has a completed or canceled status.'
            ))
        return super().update(request, *args, **kwargs)  # pylint: disable=no-member
