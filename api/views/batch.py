from api import filters as api_filters
from api import models, paginations
from api import permissions as api_permissions
from api import schemas
from api import serializers as api_serializers
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework
from rest_framework import exceptions as rest_framework_exceptions
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework import views as rest_framework_views


class BatchList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]

    filterset_class = api_filters.BatchFilter
    ordering_fields = ['created_at', 'updated_at']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    queryset = models.Batch.objects.all()
    schema = schemas.BatchListSchema()
    search_fields = ['batch_name']
    serializer_class = api_serializers.BatchSerializer

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.validated_data['start_date'] < serializer.validated_data['end_date']:
            batch = models.Batch.objects.create(
                batch_name=serializer.validated_data['batch_name'],
                start_date=serializer.validated_data['start_date'],
                end_date=serializer.validated_data['end_date'],
                shipping_cost=serializer.validated_data['shipping_cost'],
            )
        else:
            raise rest_framework_exceptions.PermissionDenied(_(
                'Start Date must be earlier than End Date.'
            ))
        return response.Response(
            {'id': batch.id},
            status=status.HTTP_201_CREATED
        )

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if not self.request.user.is_staff:
            return queryset.filter(user=self.request.user)
        return queryset


class BatchDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Batch.objects.all()
    serializer_class = api_serializers.BatchSerializer


class BatchCreate(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.BatchCreateSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        if serializer.validated_data['start_date'] < serializer.validated_date['end_date']:
            batch = models.Batch.objects.create(
                batch_name=serializer.validated_data['batch_name'],
                start_date=serializer.validated_data['start_date'],
                end_date=serializer.validated_data['end_date'],
                shipping_cost=serializer.validated_data['shipping_cost'],
            )
        else:
            raise rest_framework_exceptions.PermissionDenied(_(
                'Start Date must be earlier than End Date.'
            ))
        return response.Response(
            {'id': batch.id},
            status=status.HTTP_201_CREATED
        )
