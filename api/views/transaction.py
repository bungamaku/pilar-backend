from api import filters as api_filters
from api import models, paginations
from api import permissions as api_permissions
from api import schemas
from api import serializers as api_serializers
from api import utils as api_utils
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework
from rest_framework import exceptions as rest_framework_exceptions
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions


class TransactionList(generics.ListAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_class = api_filters.TransactionFilter
    ordering_fields = ['created_at', 'updated_at']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    queryset = models.Transaction.objects.all()
    schema = schemas.TransactionListSchema()
    search_fields = ['transaction_number', 'user_full_name']
    serializer_class = api_serializers.TransactionSerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if not self.request.user.is_staff:
            return queryset.filter(user=self.request.user)
        return queryset


class TransactionDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrOwnerReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Transaction.objects.all()
    serializer_class = api_serializers.TransactionSerializer

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if instance.transaction_status in ('005', '006', '007'):
            raise rest_framework_exceptions.PermissionDenied(_(
                'Cannot update transaction because it has a completed, canceled, or failed status.'
            ))
        if serializer.validated_data['transaction_status'] == '006':
            transaction_items = instance.transaction_items.all()
            api_utils.return_transaction_items_to_product_stock(
                transaction_items)

        if serializer.validated_data['transaction_status'] == '005':
            transaction_items = instance.transaction_items.all()
            for transaction_item in transaction_items:
                product = transaction_item.product
                product.total_profit += transaction_item.profit
                product.save()

        return super().update(request, *args, **kwargs)  # pylint: disable=no-member
