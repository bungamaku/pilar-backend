from api import models
from api import permissions as api_permissions
from api import serializers as api_serializers
from django import shortcuts
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions


class AppConfigDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [rest_framework_permissions.IsAdminUser]
    queryset = models.AppConfig.objects.all()
    serializer_class = api_serializers.AppConfigSerializer

    def get_object(self):
        obj = shortcuts.get_object_or_404(models.AppConfig)
        return obj


class HelpContactConfigDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [api_permissions.IsAdminUserOrReadOnly]
    queryset = models.HelpContactConfig.objects.all()
    serializer_class = api_serializers.HelpContactConfigSerializer

    def get_object(self):
        obj = shortcuts.get_object_or_404(models.HelpContactConfig)
        return obj


class ShipmentConfigDetail(generics.RetrieveUpdateAPIView):
    permission_classes = [api_permissions.IsAdminUserOrReadOnly]
    queryset = models.ShipmentConfig.objects.all()
    serializer_class = api_serializers.ShipmentConfigSerializer

    def get_object(self):
        obj = shortcuts.get_object_or_404(models.ShipmentConfig)
        return obj
