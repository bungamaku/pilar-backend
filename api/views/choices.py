from api import constants
from api import utils as api_utils
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework import views as rest_framework_views


class ChoicesAPIView(rest_framework_views.APIView):
    choices = None
    permission_classes = [rest_framework_permissions.IsAuthenticated]

    def get(self, request, _format=None):
        assert self.choices is not None, (
            '{} should include a `choices` attribute.'.format(
                self.__class__.__name__)
        )
        return response.Response(api_utils.map_choices(self.choices), status=status.HTTP_200_OK)


class PaymentMethodChoices(ChoicesAPIView):
    choices = constants.PAYMENT_METHOD_CHOICES


class TransactionStatusChoices(ChoicesAPIView):
    choices = constants.TRANSACTION_STATUS_CHOICES


class DonationStatusChoices(ChoicesAPIView):
    choices = constants.DONATION_STATUS_CHOICES
