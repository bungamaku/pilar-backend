from api import models
from api import permissions as api_permissions
from api import serializers as api_serializers
from api import utils as api_utils
from django import shortcuts
from django.contrib import auth
from knox import views as knox_views
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework import views as rest_framework_views
from rest_framework.authtoken import serializers as authtoken_serializers


class AuthRegister(generics.CreateAPIView):
    permission_classes = [api_permissions.IsAnonymousUser]
    serializer_class = api_serializers.UserSerializer


class AuthCredLogin(knox_views.LoginView):
    permission_classes = [rest_framework_permissions.AllowAny]
    serializer_class = authtoken_serializers.AuthTokenSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, format=None):  # pylint: disable=redefined-builtin
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        auth.login(request, user)
        return super().post(request, format)


class AuthPhoneNumberLogin(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.AllowAny]
    serializer_class = api_serializers.PhoneNumberSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = shortcuts.get_object_or_404(
            models.User,
            phone_number=serializer.validated_data['phone_number']
        )
        user.otp = api_utils.generate_otp()
        user.save()
        api_utils.send_otp(str(user.phone_number), user.otp)
        token = api_utils.generate_bearer_token(user)
        return response.Response({'token': token}, status=status.HTTP_200_OK)


class AuthOTPLogin(knox_views.LoginView):
    permission_classes = [rest_framework_permissions.AllowAny]
    serializer_class = api_serializers.OTPSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, format=None):  # pylint: disable=redefined-builtin
        http_authorization = self.request.META.get('HTTP_AUTHORIZATION')
        username = api_utils.get_username_from_bearer_token(http_authorization)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = shortcuts.get_object_or_404(models.User, username=username)
        if user.otp != serializer.validated_data['otp']:
            return response.Response(status=status.HTTP_400_BAD_REQUEST)
        auth.login(request, user)
        user.otp = ''
        user.save()
        return super().post(request, format)


class AuthResendOTP(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.AllowAny]

    def post(self, request, _format=None):
        http_authorization = self.request.META.get('HTTP_AUTHORIZATION')
        username = api_utils.get_username_from_bearer_token(http_authorization)
        user = shortcuts.get_object_or_404(models.User, username=username)
        user.otp = api_utils.generate_otp()
        user.save()
        api_utils.send_otp(str(user.phone_number), user.otp)
        return response.Response(status=status.HTTP_204_NO_CONTENT)
