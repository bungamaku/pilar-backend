from api import exceptions as api_exceptions
from api import models, paginations
from api import permissions as api_permissions
from api import serializers as api_serializers
from django.db.models import deletion
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status


class CategoryList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_fields = ['name']
    ordering_fields = ['name']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Category.objects.all()
    search_fields = ['name']
    serializer_class = api_serializers.CategorySerializer


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Category.objects.all()
    serializer_class = api_serializers.CategorySerializer

    def destroy(self, request, *_args, **_kwargs):
        instance = self.get_object()
        try:
            instance.delete()
        except deletion.ProtectedError:
            raise api_exceptions.IntegrityError(_(
                'Cannot delete category due to integrity error.'
            ))
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class SubcategoryList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_fields = ['name', 'category']
    ordering_fields = ['name']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Subcategory.objects.all()
    search_fields = ['name']
    serializer_class = api_serializers.SubcategorySerializer


class SubcategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Subcategory.objects.all()
    serializer_class = api_serializers.SubcategorySerializer

    def destroy(self, request, *_args, **_kwargs):
        instance = self.get_object()
        try:
            instance.delete()
        except deletion.ProtectedError:
            raise api_exceptions.IntegrityError(_(
                'Cannot delete subcategory due to integrity error.'
            ))
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class ProductList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_fields = ['code', 'subcategory', 'subcategory__category']
    ordering_fields = ['name', 'price', 'stock']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Product.objects.all()
    search_fields = ['code', 'name']
    serializer_class = api_serializers.ProductSerializer

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_image = None
        validated_is_hampers = False
        validated_hampers_price = 0
        try:
            validated_image = serializer.validated_data['image']
            validated_is_hampers=serializer.validated_data['is_hampers']
            validated_hampers_price = serializer.validated_data['hampers_price']
        except KeyError:
            pass
        try:
            validated_is_hampers=serializer.validated_data['is_hampers']
        except KeyError:
            pass
        try:
            validated_hampers_price = serializer.validated_data['hampers_price']
        except KeyError:
            pass
        if serializer.validated_data['preorder']:
            product = models.Product.objects.create(
                name=serializer.validated_data['name'],
                description=serializer.validated_data['description'],
                price=serializer.validated_data['price'],
                modal=serializer.validated_data['modal'],
                subcategory=models.Subcategory.objects.get(name=serializer.validated_data['subcategory']),
                total_profit=0,
                unit=serializer.validated_data['unit'],
                is_hampers=validated_is_hampers,
                hampers_price=validated_hampers_price,
                image=validated_image,
                preorder=serializer.validated_data['preorder'],
                preorder_duration=serializer.validated_data['preorder_duration'],
            )
        else:
            product = models.Product.objects.create(
                name=serializer.validated_data['name'],
                description=serializer.validated_data['description'],
                price=serializer.validated_data['price'],
                stock=serializer.validated_data['stock'],
                modal=serializer.validated_data['modal'],
                subcategory=models.Subcategory.objects.get(name=serializer.validated_data['subcategory']),
                total_profit=0,
                unit=serializer.validated_data['unit'],
                is_hampers=validated_is_hampers,
                hampers_price=validated_hampers_price,
                image=validated_image,
            )
        product.profit = (product.price - product.modal)
        product.save()
        return response.Response(
            {'id': product.id},
            status=status.HTTP_201_CREATED
        )


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.Product.objects.all()
    serializer_class = api_serializers.ProductSerializer

    def get_serializer(self, *args, **kwargs):
        # leave this intact
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()

        if (self.request != None):
            if (self.request.data.get("price") != None) and (self.request.data.get("modal") != None):
                draft_request_data = self.request.data.copy()
                profit = int(self.request.data['price']) - \
                    int(self.request.data['modal'])
                new_profit = {'profit': profit}
                draft_request_data.update(new_profit)
                kwargs["data"] = draft_request_data
                return serializer_class(*args, **kwargs)
            else:
                if (self.request.data.get("price") != None):

                    instance = self.get_object()
                    draft_request_data = self.request.data.copy()
                    profit = int(
                        self.request.data['price'])-int(instance.modal)
                    new_profit = {'profit': profit}
                    draft_request_data.update(new_profit)

                    kwargs["data"] = draft_request_data
                    return serializer_class(*args, **kwargs)
                elif (self.request.data.get("modal") != None):
                    instance = self.get_object()
                    draft_request_data = self.request.data.copy()
                    profit = int(instance.price) - \
                        int(self.request.data['modal'])
                    new_profit = {'profit': profit}
                    draft_request_data.update(new_profit)

                    kwargs["data"] = draft_request_data
                    return serializer_class(*args, **kwargs)

        return serializer_class(*args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(
            instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        return super().partial_update(request, *args, **kwargs)  # pylint: disable=no-member
