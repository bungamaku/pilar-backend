from api import models, paginations
from api import permissions as api_permissions
from api import serializers as api_serializers
from django_filters import rest_framework
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions


class UserList(generics.ListCreateAPIView):
    filter_backends = [
        rest_framework.DjangoFilterBackend,
        rest_framework_filters.OrderingFilter,
        rest_framework_filters.SearchFilter,
    ]
    filterset_fields = ['username', 'phone_number']
    ordering_fields = [
        'username', 'full_name', 'phone_number',
        'total_transactions', 'total_program_donations_goods', 'total_program_donations_cash'
    ]
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAdminUser]
    queryset = models.User.objects.all()
    search_fields = ['username', 'full_name', 'phone_number']
    serializer_class = api_serializers.UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrSelf,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.User.objects.all()
    serializer_class = api_serializers.UserSerializer

    def get_object(self):
        if self.kwargs.get('pk') == 'self':
            self.kwargs['pk'] = self.request.user.id
        return super().get_object()
