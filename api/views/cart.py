from api import models, paginations
from api import permissions as api_permissions
from api import serializers as api_serializers
from api import utils as api_utils
from django import shortcuts
from django.db import transaction as db_transaction
from django.db import utils as db_utils
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework
from home_industry import utils as home_industry_utils
from rest_framework import exceptions as rest_framework_exceptions
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework import views as rest_framework_views


class CartUpdate(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.CartUpdateSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        product = shortcuts.get_object_or_404(
            models.Product,
            id=serializer.validated_data['product']
        )
        shopping_cart = models.ShoppingCart.objects.get(user=user)
        cart_item, _created = models.CartItem.objects.get_or_create(
            product=product,
            shopping_cart=shopping_cart
        )
        validated_hampers_messages = ''
        try:
            validated_hampers_messages = serializer.validated_data['hampers_messages']
        except KeyError:
            pass
        if serializer.validated_data['quantity'] == 0:
            cart_item.delete()
        else:
            cart_item.hampers_messages = validated_hampers_messages
            cart_item.quantity = serializer.validated_data['quantity']
            cart_item.save()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class CartOverview(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]

    def get(self, request, _format=None):
        user = request.user
        shopping_cart = models.ShoppingCart.objects.get(user=user)
        item_subtotal = sum(
            (cart_item.product.price + cart_item.product.hampers_price) 
            * cart_item.quantity for cart_item in shopping_cart.cart_items.all()
        )
        shipping_costs = api_utils.get_shipping_costs(user)
        if shipping_costs is None:
            return response.Response(
                {'item_subtotal': str(item_subtotal)},
                status=status.HTTP_200_OK
            )
        return response.Response(
            {'item_subtotal': str(item_subtotal),
             'shipping_costs': str(shipping_costs)},
            status=status.HTTP_200_OK
        )


class CartCheckout(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.CartCheckoutSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        shipment_config = home_industry_utils.get_shipment_config()
        if user.sub_district.lower() != shipment_config.sub_district.lower():
            raise rest_framework_exceptions.ParseError(_(  # pylint: disable=no-member
                'Cannot process shipment to other sub-districts other than {sub_district}.'
            ).format(sub_district=shipment_config.sub_district))
        shopping_cart = models.ShoppingCart.objects.get(user=user)
        cart_items = shopping_cart.cart_items.all()
        if not cart_items.exists():
            raise rest_framework_exceptions.ParseError(_(
                'Unable to checkout because there are no items purchased.'
            ))
        api_utils.validate_product_stock(cart_items)
        transaction_status = (
            '001' if serializer.validated_data['payment_method'] == 'TRF' else '002'
        )

        transaction = models.Transaction.objects.create(
            user=user,
            payment_method=serializer.validated_data['payment_method'],
            donation=serializer.validated_data['donation'],
            transaction_status=transaction_status,
            batch=(
                None if serializer.validated_data['payment_method'] == 'TRF'
                else models.Batch.objects.filter(
                    start_date__lte=timezone.now().date(),
                    end_date__gte=timezone.now().date()).first()
            )
        )

        is_success = True
        for cart_item in cart_items:
            product = cart_item.product
            if product.stock is not None:
                try:
                    with db_transaction.atomic():
                        product.stock -= cart_item.quantity
                        profit = cart_item.quantity*product.profit
                        product.save()
                except db_utils.IntegrityError:
                    is_success = False
            models.TransactionItem.objects.create(
                transaction=transaction,
                product=product,
                quantity=cart_item.quantity,
                profit=profit,
                hampers_price=product.hampers_price,
                hampers_messages=cart_item.hampers_messages
            )
            cart_item.delete()
        if not is_success:
            transaction.transaction_status = '007'
            transaction.save()
            raise rest_framework_exceptions.APIException(_('Checkout failed.'))
        return response.Response({'transaction': transaction.id}, status=status.HTTP_200_OK)


class CartUploadPOP(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.CartUploadPOPSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        bank_account_transfer_destination = shortcuts.get_object_or_404(
            models.BankAccountTransferDestination,
            id=serializer.validated_data['bank_account_transfer_destination']
        )
        transaction = shortcuts.get_object_or_404(
            models.Transaction,
            id=serializer.validated_data['transaction'],
            user=user
        )
        if transaction.payment_method != 'TRF':
            raise rest_framework_exceptions.PermissionDenied(_(
                'The payment method for this transaction is not a transfer.'
            ))
        if transaction.transaction_status not in ('001', '002'):
            raise rest_framework_exceptions.PermissionDenied(_(
                'Cannot upload proof of payment at this stage.'
            ))
        transaction.proof_of_payment = serializer.validated_data['proof_of_payment']
        transaction.user_bank_name = serializer.validated_data['user_bank_name']
        transaction.user_bank_account_name = serializer.validated_data['user_bank_account_name']
        transaction.bank_account_transfer_destination = bank_account_transfer_destination
        transaction.update_bank_account_transfer_destination = True
        transaction.transaction_status = '002'
        transaction.batch = models.Batch.objects.filter(
            start_date__lte=timezone.now().date(),
            end_date__gte=timezone.now().date()).first()
        transaction.save()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class CartCompleteTransaction(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.CartCompleteOrCancelTransactionSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        transaction = shortcuts.get_object_or_404(
            models.Transaction,
            id=serializer.validated_data['transaction'],
            user=request.user
        )
        if transaction.transaction_status != '004':
            raise rest_framework_exceptions.PermissionDenied(_(
                'Transaction cannot be completed unless the status is "Being shipped".'
            ))
        transaction.transaction_status = '005'
        transaction.save()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class CartCancelTransaction(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.CartCompleteOrCancelTransactionSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        transaction = shortcuts.get_object_or_404(
            models.Transaction,
            id=serializer.validated_data['transaction'],
            user=user
        )
        if transaction.transaction_status not in ('001', '002'):
            raise rest_framework_exceptions.PermissionDenied(_(
                'Transaction cannot be canceled at this stage.'
            ))
        transaction_items = transaction.transaction_items.all()
        api_utils.return_transaction_items_to_product_stock(transaction_items)
        transaction.transaction_status = '006'
        transaction.save()
        return response.Response(status=status.HTTP_204_NO_CONTENT)


class ShoppingCartList(generics.ListAPIView):
    filter_backends = [rest_framework.DjangoFilterBackend]
    filterset_fields = ['user']
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [rest_framework_permissions.IsAdminUser]
    queryset = models.ShoppingCart.objects.all()
    serializer_class = api_serializers.ShoppingCartSerializer


class ShoppingCartDetail(generics.RetrieveAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrOwner,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.ShoppingCart.objects.all()
    serializer_class = api_serializers.ShoppingCartSerializer

    def get_object(self):
        if self.kwargs.get('pk') == 'self':
            self.kwargs['pk'] = models.ShoppingCart.objects.get(
                user=self.request.user).id
        return super().get_object()
