from api import models, paginations
from api import permissions as api_permissions
from api import serializers as api_serializers
from rest_framework import filters as rest_framework_filters
from rest_framework import generics
from rest_framework import permissions as rest_framework_permissions


class BankAccountTransferDestinationList(generics.ListCreateAPIView):
    filter_backends = [rest_framework_filters.SearchFilter]
    pagination_class = paginations.SmallResultsSetPagination
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.BankAccountTransferDestination.objects.all()
    search_fields = ['bank_name', 'bank_account_number', 'bank_account_name']
    serializer_class = api_serializers.BankAccountTransferDestinationSerializer


class BankAccountTransferDestinationDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [
        api_permissions.IsAdminUserOrReadOnly,
        rest_framework_permissions.IsAuthenticated,
    ]
    queryset = models.BankAccountTransferDestination.objects.all()
    serializer_class = api_serializers.BankAccountTransferDestinationSerializer
