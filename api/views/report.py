from api import reports_writer, schemas
from api import serializers as api_serializers
from django import http
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import permissions as rest_framework_permissions
from rest_framework import views as rest_framework_views


class ReportAPIView(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAdminUser]
    report_function = None
    serializer_class = None

    def get_filename(self, query_params):
        raise NotImplementedError

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)  # pylint: disable=not-callable

    def get(self, request, _format=None):
        assert self.report_function is not None, (
            '{} should include a `report_function` attribute.'.format(
                self.__class__.__name__)
        )
        assert self.report_function is not None, (
            '{} should include a `serializer_class` attribute.'.format(
                self.__class__.__name__)
        )
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        buffer = self.report_function.__func__(serializer.validated_data)
        buffer.seek(0)
        return http.FileResponse(buffer, filename=self.get_filename(serializer.validated_data))


class ReportTransaction(ReportAPIView):
    report_function = reports_writer.create_transaction_report
    schema = schemas.ReportTransactionSchema()
    serializer_class = api_serializers.ReportTransactionSerializer

    def get_filename(self, query_params):
        filename = '{}.xlsx'.format(_(  # pylint: disable=no-member
            'Transaction Report from {date_from} to {date_to} for {batch_name}'
        ).format(
            date_from=query_params.get('created_at_date_range_after', '_'),
            date_to=query_params.get(
                'created_at_date_range_before', str(timezone.now())[:10]),
            batch_name=query_params.get('batch_name', '_')
        ))
        return filename


class ReportProgramDonationCSH(ReportAPIView):
    report_function = reports_writer.create_program_donation_report_csh
    schema = schemas.ReportProgramDonationSchema()
    serializer_class = api_serializers.ReportProgramDonationSerializer

    def get_filename(self, query_params):
        filename = '{}.xlsx'.format(_(  # pylint: disable=no-member
            'Program Donation Report from {date_from} to {date_to}'
        ).format(
            date_from=query_params.get('created_at_date_range_after', '_'),
            date_to=query_params.get(
                'created_at_date_range_before', str(timezone.now())[:10])
        ))
        return filename


class ReportProgramDonationGDS(ReportAPIView):
    report_function = reports_writer.create_program_donation_report_gds
    schema = schemas.ReportProgramDonationSchema()
    serializer_class = api_serializers.ReportProgramDonationSerializer

    def get_filename(self, query_params):
        filename = '{}.xlsx'.format(_(  # pylint: disable=no-member
            'Program Donation Report from {date_from} to {date_to}'
        ).format(
            date_from=query_params.get('created_at_date_range_after', '_'),
            date_to=query_params.get(
                'created_at_date_range_before', str(timezone.now())[:10])
        ))
        return filename
