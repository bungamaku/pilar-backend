from api import models
from api import serializers as api_serializers
from django import shortcuts
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions as rest_framework_exceptions
from rest_framework import permissions as rest_framework_permissions
from rest_framework import response, status
from rest_framework import views as rest_framework_views


class DonationCreate(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.DonationCreateSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        program = shortcuts.get_object_or_404(
            models.Program,
            id=serializer.validated_data['program']
        )
        if not program.open_donation:
            raise rest_framework_exceptions.PermissionDenied(_(
                'This program is currently not accepting donations.'
            ))
        program_donation = None
        if serializer.validated_data['donation_type'] == 'CSH':
            bank_account_transfer_destination = shortcuts.get_object_or_404(
                models.BankAccountTransferDestination,
                id=serializer.validated_data['bank_account_transfer_destination']
            )
            program_donation = models.ProgramDonation.objects.create(
                user=user,
                program=program,
                donation_type='CSH',
                amount=serializer.validated_data['amount'],
                proof_of_bank_transfer=serializer.validated_data['proof_of_bank_transfer'],
                user_bank_name=serializer.validated_data['user_bank_name'],
                user_bank_account_name=serializer.validated_data['user_bank_account_name'],
                bank_account_transfer_destination=bank_account_transfer_destination
            )

        else:
            if serializer.validated_data['delivery_method'] == 'DLV':
                program_donation = models.ProgramDonation.objects.create(
                    user=user,
                    program=program,
                    donation_type='GDS',
                    goods_quantity=serializer.validated_data['goods_quantity'],
                    goods_description=serializer.validated_data['goods_description'],
                    delivery_method=serializer.validated_data['delivery_method'],
                    delivery_address=None
                )
            else:
                program_donation = models.ProgramDonation.objects.create(
                    user=user,
                    program=program,
                    donation_type='GDS',
                    goods_quantity=serializer.validated_data['goods_quantity'],
                    goods_description=serializer.validated_data['goods_description'],
                    delivery_method=serializer.validated_data['delivery_method'],
                    delivery_address=serializer.validated_data['delivery_address']
                )

        return response.Response(
            {'program_donation': program_donation.id},
            status=status.HTTP_200_OK
        )


class DonationReuploadProofOfBankTransfer(rest_framework_views.APIView):
    permission_classes = [rest_framework_permissions.IsAuthenticated]
    serializer_class = api_serializers.DonationReuploadProofOfBankTransferSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    def post(self, request, _format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        bank_account_transfer_destination = shortcuts.get_object_or_404(
            models.BankAccountTransferDestination,
            id=serializer.validated_data['bank_account_transfer_destination']
        )
        program_donation = shortcuts.get_object_or_404(
            models.ProgramDonation,
            id=serializer.validated_data['program_donation'],
            user=user
        )
        if program_donation.donation_status not in ('001', '004'):
            raise rest_framework_exceptions.PermissionDenied(_(
                'Cannot reupload proof of bank transfer at this stage.'
            ))
        if program_donation.donation_type != 'CSH':
            raise rest_framework_exceptions.PermissionDenied(_(
                'Cannot proof of bank transfer foor good donation.'
            ))
        program_donation.amount = serializer.validated_data['amount']
        program_donation.proof_of_bank_transfer = (
            serializer.validated_data['proof_of_bank_transfer']
        )
        program_donation.user_bank_name = serializer.validated_data['user_bank_name']
        program_donation.user_bank_account_name = (
            serializer.validated_data['user_bank_account_name']
        )
        program_donation.bank_account_transfer_destination = bank_account_transfer_destination
        program_donation.update_bank_account_transfer_destination = True
        program_donation.donation_status = '001'
        program_donation.save()
        return response.Response(status=status.HTTP_204_NO_CONTENT)
