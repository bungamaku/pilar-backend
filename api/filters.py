import django_filters

from api import models


class ReportTransactionFilter(django_filters.FilterSet):
    created_at_date_range = django_filters.DateFromToRangeFilter(field_name='created_at')

    class Meta:
        fields = ['created_at_date_range']
        model = models.Transaction


class ReportProgramDonationFilter(django_filters.FilterSet):
    created_at_date_range = django_filters.DateFromToRangeFilter(field_name='created_at')

    class Meta:
        fields = ['created_at_date_range']
        model = models.ProgramDonation

class TransactionFilter(django_filters.FilterSet):
    updated_at_date_range = django_filters.DateFromToRangeFilter(field_name='updated_at')

    class Meta:
        fields = [
            'transaction_number',
            'user',
            'payment_method',
            'transaction_status',
            'updated_at_date_range',
            'batch_name',
            'end_date'
        ]
        model = models.Transaction


class ProgramDonationFilter(django_filters.FilterSet):
    updated_at_date_range = django_filters.DateFromToRangeFilter(field_name='updated_at')

    class Meta:
        fields = [
            'donation_number',
            'user',
            'donation_status',
            'updated_at_date_range',
        ]
        model = models.ProgramDonation

class BatchFilter(django_filters.FilterSet):
    created_at_date_range = django_filters.DateFromToRangeFilter(field_name='created_at')

    class Meta:
        fields = ['id', 'batch_name', 'start_date', 'end_date', 'shipping_cost']
        model = models.Batch
