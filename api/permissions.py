from rest_framework import permissions


class IsAdminUserOrOwner(permissions.BasePermission):
    def has_object_permission(self, request, _view, obj):
        return bool(
            ((request.user) and (request.user.is_staff)) or
            (obj.user == request.user)
        )


class IsAdminUserOrOwnerReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, _view, obj):
        return bool(
            ((request.user) and (request.user.is_staff)) or
            ((obj.user == request.user) and (request.method in permissions.SAFE_METHODS))
        )


class IsAdminUserOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, _view):
        return bool(
            ((request.user) and (request.user.is_staff)) or
            (request.method in permissions.SAFE_METHODS)
        )


class IsAdminUserOrSelf(permissions.BasePermission):
    def has_object_permission(self, request, _view, obj):
        return bool(
            ((request.user) and (request.user.is_staff)) or
            ((request.user) and (obj == request.user))
        )


class IsAnonymousUser(permissions.BasePermission):
    def has_permission(self, request, _view):
        return bool(request.user.is_anonymous)
