import decimal
import uuid

from django.contrib.auth import models as auth_models
from django.contrib.postgres import fields
from django.core import validators
from django.db import models as db_models
from django.utils.translation import gettext_lazy as _
from phonenumber_field import modelfields
from solo import models as solo_models

from api import constants, utils


class User(auth_models.AbstractUser):
    REQUIRED_FIELDS = ['phone_number']

    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    first_name = None
    last_name = None
    full_name = db_models.CharField(max_length=200, verbose_name=_('full name'))
    phone_number = modelfields.PhoneNumberField(unique=True, verbose_name=_('phone number'))
    address = db_models.CharField(max_length=200, verbose_name=_('address'))
    neighborhood = db_models.CharField(
        max_length=3,
        validators=[validators.int_list_validator(sep=''), validators.MinLengthValidator(3)],
        verbose_name=_('neighborhood')
    )
    hamlet = db_models.CharField(
        max_length=3,
        validators=[validators.int_list_validator(sep=''), validators.MinLengthValidator(3)],
        verbose_name=_('hamlet')
    )
    urban_village = db_models.CharField(max_length=100, verbose_name=_('urban village'))
    sub_district = db_models.CharField(max_length=100, verbose_name=_('sub-district'))
    profile_picture = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('profile picture')
    )
    otp = db_models.CharField(blank=True, max_length=6, null=True, verbose_name=_('OTP'))

    class Meta:
        ordering = ['username', 'id']
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.username


class BankAccountTransferDestination(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    bank_name = db_models.CharField(max_length=100, verbose_name=_('bank name'))
    bank_code_number = db_models.CharField(max_length=100, verbose_name=_('bank code number'), default='000')
    bank_account_number = db_models.CharField(max_length=100, verbose_name=_('bank account number'))
    bank_account_name = db_models.CharField(max_length=200, verbose_name=_('bank account name'))

    class Meta:
        ordering = ['bank_name', 'id']
        unique_together = ['bank_name', 'bank_account_number']
        verbose_name = _('bank account transfer destination')
        verbose_name_plural = _('bank account transfer destinations')

    def __str__(self):
        return '{}: {} - {}'.format(self.bank_name, self.bank_code_number, self.bank_account_number)


class Category(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    name = fields.CICharField(max_length=50, unique=True, verbose_name=_('name'))
    image = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('image')
    )

    class Meta:
        ordering = ['name', 'id']
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return self.name


class Subcategory(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    name = fields.CICharField(max_length=50, unique=True, verbose_name=_('name'))
    category = db_models.ForeignKey(
        'api.Category',
        on_delete=db_models.CASCADE,
        related_name='subcategories',
        verbose_name=_('category')
    )
    image = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('image')
    )

    class Meta:
        ordering = ['category', 'name', 'id']
        verbose_name = _('subcategory')
        verbose_name_plural = _('subcategories')

    def __str__(self):
        return self.name


class Product(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    code = db_models.CharField(
        default=utils.generate_code,
        max_length=6,
        unique=True,
        verbose_name=_('code')
    )
    name = db_models.CharField(max_length=200, verbose_name=_('name'))
    subcategory = db_models.ForeignKey(
        'api.Subcategory',
        on_delete=db_models.PROTECT,
        related_name='products',
        verbose_name=_('subcategory')
    )
    description = db_models.TextField(verbose_name=_('description'))
    price = db_models.DecimalField(
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('price')
    )
    stock = db_models.PositiveIntegerField(blank=True, null=True, verbose_name=_('stock'))
   
    modal = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('modal')
    )
    profit= db_models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('profit')
    )
    image = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('image')
    )
    total_profit = db_models.DecimalField(
        blank=True,
        null=True,
        default=decimal.Decimal('0'),
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('total profit')
    )
    is_hampers = db_models.BooleanField(default=False, verbose_name=_('the product is hampers package'))
    hampers_price = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0.00'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.00'))],
        verbose_name=_('hampers price')
    )
    unit = db_models.CharField(default='buah', max_length=200, verbose_name=_('unit'))
    preorder = db_models.BooleanField(blank=True, null=True, default=False, verbose_name=_('preorder'))
    preorder_duration = db_models.PositiveIntegerField(
        blank=True,
        null=True,
        default=0,
        verbose_name=_('preorder duration')
    )
    class Meta:
        ordering = ['subcategory', 'name', 'code', 'id']
        verbose_name = _('product')
        verbose_name_plural = _('products')

    def __str__(self):
        return self.code


class ShoppingCart(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    user = db_models.OneToOneField('api.User', on_delete=db_models.CASCADE, verbose_name=_('user'))

    class Meta:
        ordering = ['user', 'id']
        verbose_name = _('shopping cart')
        verbose_name_plural = _('shopping carts')

    def __str__(self):
        return self.user.username


class CartItem(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    shopping_cart = db_models.ForeignKey(
        'api.ShoppingCart',
        on_delete=db_models.CASCADE,
        related_name='cart_items',
        verbose_name=_('shopping cart')
    )
    product = db_models.ForeignKey(
        'api.Product',
        on_delete=db_models.CASCADE,
        related_name='cart_items',
        verbose_name=_('product')
    )
    quantity = db_models.PositiveIntegerField(default=0, verbose_name=_('quantity'))
    hampers_messages = db_models.CharField(default='', max_length=100, verbose_name=_('hampers messages'))

    class Meta:
        ordering = ['shopping_cart', 'product', 'id']
        unique_together = ['shopping_cart', 'product']
        verbose_name = _('cart item')
        verbose_name_plural = _('cart items')

    def __str__(self):
        return self.product.code


class Transaction(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    transaction_number = db_models.CharField(
        default=utils.generate_transaction_number,
        max_length=8,
        unique=True,
        verbose_name=_('transaction number')
    )
    user = db_models.ForeignKey(
        'api.User',
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='transactions',
        verbose_name=_('user')
    )
    user_full_name = db_models.CharField(max_length=200, verbose_name=_('user full name'))
    user_phone_number = modelfields.PhoneNumberField(verbose_name=_('user phone number'))
    shipping_address = db_models.CharField(max_length=200, verbose_name=_('shipping address'))
    shipping_neighborhood = db_models.CharField(
        max_length=3,
        validators=[validators.int_list_validator(sep=''), validators.MinLengthValidator(3)],
        verbose_name=_('shipping neighborhood')
    )
    shipping_hamlet = db_models.CharField(
        max_length=3,
        validators=[validators.int_list_validator(sep=''), validators.MinLengthValidator(3)],
        verbose_name=_('shipping hamlet')
    )
    shipping_urban_village = db_models.CharField(
        max_length=100,
        verbose_name=_('shipping urban village')
    )
    shipping_sub_district = db_models.CharField(
        max_length=100,
        verbose_name=_('shipping sub-district')
    )
    shipping_costs = db_models.DecimalField(
        blank=True,
        decimal_places=2,
        max_digits=12,
        null=True,
        validators=[validators.MinValueValidator(decimal.Decimal('0'))],
        verbose_name=_('shipping costs')
    )
    payment_method = db_models.CharField(
        choices=constants.PAYMENT_METHOD_CHOICES,
        max_length=3,
        verbose_name=_('payment method')
    )
    donation = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0'))],
        verbose_name=_('donation')
    )
    transaction_status = db_models.CharField(
        choices=constants.TRANSACTION_STATUS_CHOICES,
        max_length=3,
        verbose_name=_('transaction status')
    )
    proof_of_payment = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('proof of payment')
    )
    user_bank_name = db_models.CharField(
        blank=True,
        max_length=100,
        null=True,
        verbose_name=_('user bank name')
    )
    user_bank_account_name = db_models.CharField(
        blank=True,
        max_length=200,
        null=True,
        verbose_name=_('user bank account name')
    )
    bank_account_transfer_destination = db_models.ForeignKey(
        'api.BankAccountTransferDestination',
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='transactions',
        verbose_name=_('bank account transfer destination')
    )
    transfer_destination_bank_name = db_models.CharField(
        blank=True,
        max_length=100,
        null=True,
        verbose_name=_('transfer destination bank name')
    )
    transfer_destination_bank_account_name = db_models.CharField(
        blank=True,
        max_length=200,
        null=True,
        verbose_name=_('transfer destination bank account name')
    )
    transfer_destination_bank_account_number = db_models.CharField(
        blank=True,
        max_length=100,
        null=True,
        verbose_name=_('transfer destination bank account number')
    )
    transfer_destination_bank_code_number = db_models.CharField(
        blank=True,
        max_length=100,
        null=True,
        verbose_name=_('transfer destination bank code number')
    )
    created_at = db_models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name=_('created at')
    )
    updated_at = db_models.DateTimeField(auto_now=True, db_index=True, verbose_name=_('updated at'))

    batch = db_models.ForeignKey(
        'api.Batch',
        null=True,
        blank=True,
        on_delete=db_models.CASCADE,
        related_name='transaction',
        verbose_name=_('batch')
        )

    batch_name = db_models.CharField(max_length=200, verbose_name=_('batch name'))

    end_date = db_models.DateField(
        null=True,
        verbose_name=_('end date')
    )
 
    class Meta:
        ordering = ['-updated_at', '-created_at', 'transaction_number', 'id']
        verbose_name = _('transaction')
        verbose_name_plural = _('transactions')

    def __str__(self):
        return self.transaction_number


class TransactionItem(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    transaction = db_models.ForeignKey(
        'api.Transaction',
        on_delete=db_models.CASCADE,
        related_name='transaction_items',
        verbose_name=_('transaction')
    )
    product = db_models.ForeignKey(
        'api.Product',
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='transaction_items',
        verbose_name=_('product')
    )
    product_name = db_models.CharField(max_length=200, verbose_name=_('product name'))
    product_price = db_models.DecimalField(
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('product price')
    )
    profit= db_models.DecimalField(
        blank=True,
        null=True,
        default=decimal.Decimal('0'),
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('total profit')
    )
    
    hampers_price = db_models.DecimalField(
        default=decimal.Decimal('0.00'),
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.00'))],
        verbose_name=_('hampers price')
    )
    hampers_messages = db_models.CharField(default='', max_length=100, verbose_name=_('hampers messages'))
    quantity = db_models.PositiveIntegerField(verbose_name=_('quantity'))

    class Meta:
        ordering = ['transaction', 'product', 'id']
        verbose_name = _('transaction item')
        verbose_name_plural = _('transaction items')

    def __str__(self):
        return self.product_name


class Program(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    code = db_models.CharField(
        default=utils.generate_code,
        max_length=6,
        unique=True,
        verbose_name=_('code')
    )
    name = db_models.CharField(max_length=200, verbose_name=_('name'))
    description = db_models.TextField(verbose_name=_('description'))
    start_date_time = db_models.DateTimeField(
        blank=True,
        db_index=True,
        null=True,
        verbose_name=_('start date and time')
    )
    end_date_time = db_models.DateTimeField(
        blank=True,
        null=True,
        verbose_name=_('end date and time')
    )
    location = db_models.CharField(
        blank=True,
        max_length=200,
        null=True,
        verbose_name=_('location')
    )
    speaker = db_models.CharField(blank=True, max_length=200, null=True, verbose_name=_('speaker'))
    open_donation = db_models.BooleanField(default=True, verbose_name=_('open for donation'))
    program_minutes = db_models.FileField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        validators=[validators.FileExtensionValidator(allowed_extensions=['pdf'])],
        verbose_name=_('program minutes')
    )
    poster_image = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('poster image')
    )
    link = db_models.URLField(blank=True, null=True, verbose_name=_('link'))

    class Meta:
        ordering = ['-start_date_time', '-end_date_time', 'name', 'code', 'id']
        verbose_name = _('program')
        verbose_name_plural = _('programs')

    def __str__(self):
        return self.code


class ProgramDonation(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    donation_number = db_models.CharField(
        default=utils.generate_donation_number,
        max_length=6,
        unique=True,
        verbose_name=_('donation number')
    )
    program = db_models.ForeignKey(
        'api.Program',
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='program_donations',
        verbose_name=_('program')
    )
    user = db_models.ForeignKey(
        'api.User',
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='program_donations',
        verbose_name=_('user')
    )
    user_full_name = db_models.CharField(max_length=200, verbose_name=_('user full name'))
    user_phone_number = modelfields.PhoneNumberField(verbose_name=_('user phone number'))
    program_name = db_models.CharField(max_length=200, verbose_name=_('program name'))
    amount = db_models.DecimalField(
        blank=True,
        null=True,
        decimal_places=2,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0.01'))],
        verbose_name=_('amount')
    )
    donation_status = db_models.CharField(
        choices=constants.DONATION_STATUS_CHOICES,
        default='001',
        max_length=3,
        verbose_name=_('donation status')
    )
    donation_type = db_models.CharField(
        choices=constants.DONATION_TYPE_CHOICES,
        default='CSH',
        max_length=3,
        verbose_name=_('donation type')
    )
    proof_of_bank_transfer = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('proof of bank transfer')
    )
    user_bank_name = db_models.CharField(
        blank=True,
        null=True,
        max_length=100,
        verbose_name=_('user bank name')
    )
    user_bank_account_name = db_models.CharField(
        blank=True,
        null=True,
        max_length=200,
        verbose_name=_('user bank account name')
    )
    bank_account_transfer_destination = db_models.ForeignKey(
        'api.BankAccountTransferDestination',
        blank=True,
        null=True,
        on_delete=db_models.SET_NULL,
        related_name='program_donations',
        verbose_name=_('bank account transfer destination')
    )
    transfer_destination_bank_name = db_models.CharField(
        blank=True,
        null=True,
        max_length=100,
        verbose_name=_('transfer destination bank name')
    )
    transfer_destination_bank_account_name = db_models.CharField(
        blank=True,
        null=True,
        max_length=200,
        verbose_name=_('transfer destination bank account name')
    )
    transfer_destination_bank_account_number = db_models.CharField(
        blank=True,
        null=True,
        max_length=100,
        verbose_name=_('transfer destination bank account number')
    )
    transfer_destination_bank_code_number = db_models.CharField(
        blank=True,
        null=True,
        max_length=100,
        verbose_name=_('transfer destination bank code number')
    )
    goods_quantity = db_models.DecimalField(
        blank=True,
        null=True,
        decimal_places=0,
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('1'))],
        verbose_name=_('goods quantity')
    )
    goods_description = db_models.CharField(
        blank=True,
        null=True,
        max_length=200,
        verbose_name=_('goods description')
    )
    delivery_method = db_models.CharField(
        choices=constants.DONATION_GOODS_DELIVERY_METHOD_CHOICES,
        blank=True,
        null=True,
        max_length=3,
        verbose_name=_('goods delivery method')
    )
    delivery_address = db_models.CharField(
        blank=True,
        null=True,
        max_length=200,
        verbose_name=_('goods delivery address')
    )
    created_at = db_models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name=_('created at')
    )
    updated_at = db_models.DateTimeField(auto_now=True, db_index=True, verbose_name=_('updated at'))

    class Meta:
        ordering = ['-updated_at', '-created_at', 'program', 'donation_number', 'id']
        verbose_name = _('program donation')
        verbose_name_plural = _('program donations')

    def __str__(self):
        return self.donation_number


class AppConfig(solo_models.SingletonModel):
    send_sms = db_models.BooleanField(default=False, verbose_name=_('send SMS'))

    class Meta:
        verbose_name = _('application configuration')
        verbose_name_plural = _('application configurations')

    def __str__(self):
        return 'Application Configuration'


class HelpContactConfig(solo_models.SingletonModel):
    email = db_models.EmailField(verbose_name=_('email'))

    class Meta:
        verbose_name = _('help contact configuration')
        verbose_name_plural = _('help contact configurations')

    def __str__(self):
        return 'Help Contact Configuration'


class ShipmentConfig(solo_models.SingletonModel):
    hamlet = db_models.CharField(
        max_length=3,
        validators=[validators.int_list_validator(sep=''), validators.MinLengthValidator(3)],
        verbose_name=_('hamlet')
    )
    urban_village = db_models.CharField(
        max_length=100,
        verbose_name=_('urban village')
    )
    sub_district = db_models.CharField(
        max_length=100,
        verbose_name=_('sub-district')
    )
    same_hamlet_costs = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0'))],
        verbose_name=_(
            'shipping costs if the hamlet, urban village, and sub-district are the same as seller'
        )
    )
    same_urban_village_different_hamlet_costs = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0'))],
        verbose_name=_(
            'shipping costs if the urban village and sub-district are the same as seller'
        )
    )
    same_sub_district_different_urban_village_costs = db_models.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        max_digits=12,
        validators=[validators.MinValueValidator(decimal.Decimal('0'))],
        verbose_name=_('shipping costs if the sub-district is the same as seller')
    )

    class Meta:
        verbose_name = _('shipment configuration')
        verbose_name_plural = _('shipment configurations')

    def __str__(self):
        return 'Shipment Configuration'


class Batch(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))

    batch_name = db_models.CharField(max_length=200, verbose_name=_('name'))

    start_date = db_models.DateField(
        blank=False,
        verbose_name=_('start date')
    )
    end_date = db_models.DateField(
        blank=False,
        verbose_name=_('end date')
    )

    shipping_cost = db_models.IntegerField(
        blank=True,
        verbose_name=_('shipping cost'),
        null=True
    )

    class Meta:
        ordering = ['-start_date', '-end_date', 'batch_name', 'id']
        verbose_name = _('batch')
        verbose_name_plural = _('batches')

    def __str__(self):
        return self.batch_name

class ProgramProgress(db_models.Model):
    id = db_models.UUIDField(default=uuid.uuid4, primary_key=True, verbose_name=_('ID'))
    date = db_models.DateField(
        blank=False,
        verbose_name=_('date')
    )
    description = fields.CICharField(max_length=200, verbose_name=_('description'))
    image = db_models.ImageField(
        blank=True,
        null=True,
        upload_to=utils.get_upload_file_path,
        verbose_name=_('image')
    )
    program = db_models.ForeignKey(
        'api.Program',
        on_delete=db_models.CASCADE,
        related_name='program',
        verbose_name=_('program')
    )

    class Meta:
        ordering = ['-date', 'id']
        verbose_name = _('program progress')
        verbose_name_plural = _('program progress')

    def __str__(self):
        return self.name
