import datetime
import decimal
import tempfile
import uuid
from os import name
from unittest import mock

import jwt
from django import conf
from django import test as django_test
from django import urls
from django.core import management
from django.utils import timezone
from PIL import Image
from rest_framework import exceptions, status
from rest_framework import test as rest_framework_test

from api import models, seeds, utils, views


def create_tmp_image():
    image = Image.new('RGBA', size=(10, 10), color=(0, 0, 0))
    tmp_file = tempfile.NamedTemporaryFile(suffix='.png')
    image.save(tmp_file)
    return tmp_file


def create_user(user_data):
    user = models.User.objects.create(**user_data)
    user.set_password(user_data['password'])
    user.save()
    return user


def get_http_authorization(username, password):
    client = rest_framework_test.APIClient()
    data = {
        'username': username,
        'password': password,
    }
    url = urls.reverse('auth-cred-login')
    response = client.post(url, data, format='json')
    return 'Token {}'.format(response.data['token'])


def request(method, url_name, data=None, format='json', http_authorization=None, url_args=None): # pylint: disable=redefined-builtin,too-many-arguments
    client = rest_framework_test.APIClient()
    response = None
    url = urls.reverse(url_name, args=url_args)
    if http_authorization is not None:
        client.credentials(HTTP_AUTHORIZATION=http_authorization)
    if method == 'GET':
        response = client.get(url)
    elif method == 'POST':
        response = client.post(url, data, format=format)
    elif method == 'PUT':
        response = client.put(url, data, format=format)
    elif method == 'PATCH':
        response = client.patch(url, data, format=format)
    elif method == 'DELETE':
        response = client.delete(url)
    return response


class CommandsTest(django_test.TestCase):
    def test_createorupdateapiconfig_command(self):
        management.call_command('createorupdateapiconfig')
        self.assertTrue(models.AppConfig.objects.count(), 1)
        self.assertTrue(models.HelpContactConfig.objects.count(), 1)
        self.assertTrue(models.ShipmentConfig.objects.count(), 1)


class UtilsTest(django_test.TestCase):
    def test_generate_bearer_token(self):
        user = models.User.objects.create(**seeds.USER_DATA)
        token = utils.generate_bearer_token(user)
        decoded_jwt = jwt.decode(token, conf.settings.SECRET_KEY, algorithms=['HS256'])
        self.assertTrue('exp' in decoded_jwt)
        self.assertTrue('username' in decoded_jwt)

    def test_generate_code(self):
        code = utils.generate_code()
        self.assertEqual(len(code), 6)

    def test_generate_otp(self):
        otp = utils.generate_otp()
        self.assertEqual(len(otp), 6)

    def test_generate_transaction_number(self):
        transaction_number = utils.generate_transaction_number()
        self.assertEqual(len(transaction_number), 8)

    def test_get_username_from_bearer_token_success(self):
        user = models.User.objects.create(**seeds.USER_DATA)
        token = utils.generate_bearer_token(user)
        http_authorization = 'Bearer {}'.format(token)
        username = utils.get_username_from_bearer_token(http_authorization)
        self.assertEqual(username, seeds.USER_DATA['username'])

    def test_get_shipping_costs(self):
        shipment_config = models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        user = create_user(seeds.USER_DATA)
        shipping_costs = utils.get_shipping_costs(user)
        self.assertEqual(shipping_costs, decimal.Decimal(shipment_config.same_hamlet_costs))
        user.hamlet = '002'
        user.save()
        shipping_costs = utils.get_shipping_costs(user)
        self.assertEqual(
            shipping_costs,
            decimal.Decimal(shipment_config.same_urban_village_different_hamlet_costs)
        )
        user.urban_village = 'Another Urban Village'
        user.save()
        shipping_costs = utils.get_shipping_costs(user)
        self.assertEqual(
            shipping_costs,
            decimal.Decimal(shipment_config.same_sub_district_different_urban_village_costs)
        )
        user.sub_district = 'Another Sub-District'
        user.save()
        shipping_costs = utils.get_shipping_costs(user)
        self.assertIsNone(shipping_costs)

    def test_get_upload_file_path(self):
        instance = models.User.objects.create(**seeds.USER_DATA)
        filename = 'dummy'
        upload_file_path = utils.get_upload_file_path(instance, filename)
        self.assertRegex(upload_file_path, r'^uploads/user/.+_dummy$')

    def test_get_username_from_bearer_token_fail(self):
        http_authorization = None
        with self.assertRaises(exceptions.NotAuthenticated):
            utils.get_username_from_bearer_token(http_authorization)
        http_authorization = 'Bearers token'
        with self.assertRaises(exceptions.AuthenticationFailed):
            utils.get_username_from_bearer_token(http_authorization)
        http_authorization = 'Bearer token'
        with self.assertRaises(exceptions.AuthenticationFailed):
            utils.get_username_from_bearer_token(http_authorization)
        encoded_jwt = jwt.encode(
            {'username': seeds.USER_DATA['username']},
            conf.settings.SECRET_KEY,
            algorithm='HS256'
        ).decode('utf-8')
        http_authorization = 'Bearer {}'.format(encoded_jwt)
        with self.assertRaises(exceptions.AuthenticationFailed):
            utils.get_username_from_bearer_token(http_authorization)

    def test_map_choices(self):
        choices = [
            ('1', 'Dummy description 1'),
            ('2', 'Dummy description 2'),
        ]
        mapped_choices = utils.map_choices(choices)
        self.assertIsInstance(mapped_choices, list)
        self.assertEqual(len(mapped_choices), len(choices))

    def get_batch_transaction_success(self):
        transaction = models.Transaction.objects.create(**seeds.TRANSACTION_DATA)
        transaction.created_at = datetime.datetime.now()
        batch = models.Batch.objects.create(**seeds.BATCH_DATA)
        batch_transaction = utils.get_batch_transaction(transaction)
        self.assertEqual(batch.batch_name, batch_transaction.batch_name)

    def get_batch_transaction_fail(self):
        transaction = models.Transaction.objects.create(**seeds.TRANSACTION_DATA)
        transaction.created_at = datetime.datetime.now()
        batch = models.Batch.objects.create('Batch 2', '2020-10-08', '2020-10-08', 19000)
        batch_transactio = utils.get_batch_transaction(transaction)
        self.assertEqual(None, batch_transaction.batch_name)




class AuthTest(rest_framework_test.APITestCase):
    def setUp(self):
        data = seeds.USER_DATA
        url = urls.reverse('auth-register')
        self.client.post(url, data, format='json')

    def test_user_authentication_with_username_password_success(self):
        data = {
            'username': seeds.USER_DATA['username'],
            'password': seeds.USER_DATA['password'],
        }
        response = request(
            'POST',
            'auth-cred-login',
            data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('token'))

    @mock.patch('home_industry.utils.send_sms', return_value=None)
    def test_user_authentication_with_phone_number_success(self, mock_send_sms):
        data = {
            'phone_number': seeds.USER_DATA['phone_number'],
        }
        response = request(
            'POST',
            'auth-phone-number-login',
            data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('token'))
        self.assertEqual(mock_send_sms.call_count, 1)
        user = models.User.objects.get(username=seeds.USER_DATA['username'])
        user.otp = '123456'
        user.save()
        data = {
            'otp': '123456',
        }
        response = request(
            'POST',
            'auth-otp-login',
            data,
            http_authorization='Bearer {}'.format(response.data['token'])
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('token'))

    @mock.patch('home_industry.utils.send_sms', return_value=None)
    def test_user_authentication_fail(self, mock_send_sms):
        data = {
            'phone_number': seeds.USER_DATA['phone_number'],
        }
        response = request(
            'POST',
            'auth-phone-number-login',
            data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.data.get('token'))
        self.assertEqual(mock_send_sms.call_count, 1)
        user = models.User.objects.get(username=seeds.USER_DATA['username'])
        user.otp = '123456'
        user.save()
        data = {
            'otp': '654321',
        }
        response = request(
            'POST',
            'auth-otp-login',
            data,
            http_authorization='Bearer {}'.format(response.data['token'])
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @mock.patch('home_industry.utils.send_sms', return_value=None)
    def test_resend_otp_success(self, mock_send_sms):
        data = {
            'phone_number': seeds.USER_DATA['phone_number'],
        }
        response = request(
            'POST',
            'auth-phone-number-login',
            data
        )
        response = request(
            'POST',
            'auth-resend-otp',
            http_authorization='Bearer {}'.format(response.data['token'])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(mock_send_sms.call_count, 2)

    def test_resend_otp_fail(self):
        response = request(
            'POST',
            'auth-resend-otp'
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class CartTest(rest_framework_test.APITestCase): # pylint: disable=too-many-instance-attributes
    def setUp(self):
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        self.user = create_user(seeds.USER_DATA)
        self.user_http_authorization = get_http_authorization(
            seeds.USER_DATA['username'],
            seeds.USER_DATA['password']
        )
        self.bank_account_transfer_destination = (
            models.BankAccountTransferDestination.objects.create(
                **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
            )
        )
        self.category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.subcategory = models.Subcategory.objects.create(**dict(
            seeds.CATEGORY_DATA,
            category=self.category
        ))
        self.product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA,
            subcategory=self.subcategory,
            profit=1000
        ))
        self.shopping_cart = models.ShoppingCart.objects.get(user=self.user)
        self.proof_of_payment_file = create_tmp_image()
        self.batch = models.Batch.objects.create(**seeds.BATCH_DATA)

    def test_cart_checkout_success(self):
        data = {
            'product': self.product.id,
            'quantity': 1,
            'hampers_messages': 'Selamat Lebaran',
        }
        response = request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(self.shopping_cart.cart_items.count(), 1)
        response = request(
            'GET',
            'cart-overview',
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('item_subtotal' in response.data)
        self.assertTrue('shipping_costs' in response.data)
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Transaction.objects.count(), 1)

    def test_cart_checkout_fail(self):
        data = {
            'product': self.product.id,
            'quantity': 1,
            'hampers_messages': 'Selamat Lebaran',
        }
        response = request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(self.shopping_cart.cart_items.count(), 1)
        data = {
            'product': self.product.id,
            'quantity': 0,
        }
        response = request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(self.shopping_cart.cart_items.count(), 0)
        self.user.sub_district = 'Another Sub-District'
        self.user.save()
        response = request(
            'GET',
            'cart-overview',
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('item_subtotal' in response.data)
        self.assertFalse('shipping_costs' in response.data)
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.user.sub_district = seeds.USER_DATA['sub_district']
        self.user.save()
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'product': self.product.id,
            'quantity': 20,
            'hampers_messages': 'Selamat Lebaran',
        }
        request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Transaction.objects.count(), 0)

    def test_cart_complete_transaction(self):
        data = {
            'product': self.product.id,
            'quantity': 1,
            'hampers_messages': 'Selamat Lebaran',
        }
        request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        transaction = models.Transaction.objects.get(id=response.data['transaction'])
        transaction.transaction_status = '004'
        transaction.save()
        data = {
            'transaction': transaction.id,
        }
        response = request(
            'POST',
            'cart-complete-transaction',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = request(
            'POST',
            'cart-complete-transaction',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(transaction.batch, None)


    def test_cart_cancel_transaction(self):
        data = {
            'product': self.product.id,
            'quantity': 1,
            'hampers_messages': 'Selamat Lebaran',
        }
        request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization
        )
        data = {
            'transaction': response.data['transaction'],
        }
        response = request(
            'POST',
            'cart-cancel-transaction',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = request(
            'POST',
            'cart-cancel-transaction',
            data,
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # def test_cart_upload_pop_fail(self):
    #     data = {
    #         'product': self.product.id,
    #         'quantity': 1,
    #     }
    #     request(
    #         'POST',
    #         'cart-update',
    #         data,
    #         http_authorization=self.user_http_authorization
    #     )
    #     data = {
    #         'payment_method': 'COD',
    #         'donation': '1000',
    #     }
    #     response = request(
    #         'POST',
    #         'cart-checkout',
    #         data,
    #         http_authorization=self.user_http_authorization
    #     )
    #     data = {
    #         'product': self.product.id,
    #         'quantity': 1,
    #     }
    #     request(
    #         'POST',
    #         'cart-update',
    #         data,
    #         http_authorization=self.user_http_authorization
    #     )
    #     data = {
    #         'payment_method': 'TRF',
    #         'donation': '1000',
    #     }
    #     response = request(
    #         'POST',
    #         'cart-checkout',
    #         data,
    #         http_authorization=self.user_http_authorization
    #     )
    #     transaction = models.Transaction.objects.get(id=response.data['transaction'])
    #     transaction.transaction_status = '003'
    #     transaction.save()

    @mock.patch('api.utils.validate_product_stock', return_value=None)
    def test_cart_checkout_race_condition(self, mock_validate_product_stock):
        data = {
            'product': self.product.id,
            'quantity': 20,
            'hampers_messages': 'Selamat Lebaran',
        }
        request(
            'POST',
            'cart-update',
            data,
            http_authorization=self.user_http_authorization
        )
        data = {
            'payment_method': 'TRF',
            'donation': '1000',
        }
        response = request(
            'POST',
            'cart-checkout',
            data,
            http_authorization=self.user_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)
        self.assertEqual(mock_validate_product_stock.call_count, 1)


class DonationCashTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.user = create_user(seeds.USER_DATA)
        self.user_http_authorization = get_http_authorization(
            seeds.USER_DATA['username'],
            seeds.USER_DATA['password']
        )
        self.bank_account_transfer_destination = (
            models.BankAccountTransferDestination.objects.create(
                **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
            )
        )
        self.proof_of_bank_transfer_file = create_tmp_image()


    def test_donation_reupload_proof_of_bank_transfer_success(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=program
        ))
        program_donation.save()

    def test_donation_reupload_proof_of_bank_transfer_fail(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=program
        ))
        program_donation.donation_status = '002'
        program_donation.save()

class DonationDeliveryTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.user = create_user(seeds.USER_DATA)
        self.user_http_authorization = get_http_authorization(
            seeds.USER_DATA['username'],
            seeds.USER_DATA['password']
        )


    def test_donation_goods_create_success(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_GOODS_DATA,
            user=self.user,
            program=program,
        ))
        program_donation.donation_type = 'GDS'
        program_donation.save()

    def test_donation_goods_create_fail(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_GOODS_DATA,
            user=self.user,
            program=program,
        ))
        program_donation.donation_type = 'GDS'
        program_donation.donation_status = '002'
        program_donation.save()

    def test_delivery_method(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_GOODS_DATA_DLV,
            user=self.user,
            program=program,
        ))
        program_donation.donation_type = 'GDS'
        self.assertEqual(program_donation.delivery_address, None)
        self.assertEqual(program_donation.delivery_method, 'DLV')


class ReportViewsTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        models.Batch.objects.create(**seeds.BATCH_DATA)
        models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )

    def test_get_filename_not_implemented_error(self):
        with self.assertRaises(NotImplementedError):
            view = views.ReportAPIView()
            view.get_filename({})

    def test_transaction_report_success(self):
        user = create_user(seeds.USER_DATA)
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.CATEGORY_DATA,
            category=category
        ))
        product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA, subcategory=subcategory
        ))
        transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA, user=user
        ))
        models.TransactionItem.objects.create(**dict(
            seeds.TRANSACTION_ITEM_DATA,
            transaction=transaction,
            product=product
        ))
        transaction.transaction_status = '005'
        transaction.save()
        response = request(
            'GET',
            'transaction-report',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.delete()
        response = request(
            'GET',
            'transaction-report',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_program_donation_report_CSH_success(self):
        user = create_user(seeds.USER_DATA)
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=user,
            program=program
        ))
        program_donation.donation_status = '002'
        program_donation.save()
        response = request(
            'GET',
            'program-donation-report-csh',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        program.delete()
        response = request(
            'GET',
            'program-donation-report-csh',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_program_donation_report_GDS_success(self):
        user = create_user(seeds.USER_DATA)
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=user,
            program=program
        ))
        program_donation.donation_status = '002'
        program_donation.donation_type = 'GDS'
        program_donation.save()
        response = request(
            'GET',
            'program-donation-report-gds',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        program.delete()
        response = request(
            'GET',
            'program-donation-report-gds',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_user_model_string_representation(self):
        user = create_user(seeds.USER_DATA)
        self.assertEqual(str(user), seeds.USER_DATA['username'])

    def test_user_list_success(self):
        response = request(
            'GET',
            'user-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_user_detail_success(self):
        response = request(
            'GET',
            'user-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=['self']
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_user_success(self):
        data = seeds.USER_DATA
        response = request(
            'POST',
            'user-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.User.objects.count(), 2)
        self.assertEqual(
            models.User.objects.get(id=response.data['id']).full_name,
            data['full_name']
        )

    def test_create_user_fail(self):
        response = request(
            'POST',
            'user-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.User.objects.count(), 1)

    def test_update_user_success(self):
        user = create_user(seeds.USER_DATA)
        data = {
            'full_name': 'Dummy',
        }
        response = request(
            'PATCH',
            'user-detail',
            data,
            http_authorization=get_http_authorization(
                seeds.USER_DATA['username'],
                seeds.USER_DATA['password']
            ),
            url_args=[user.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.User.objects.get(id=user.id).full_name, data['full_name'])
        data = seeds.USER_DATA
        response = request(
            'PUT',
            'user-detail',
            data,
            http_authorization=get_http_authorization(
                seeds.USER_DATA['username'],
                seeds.USER_DATA['password']
            ),
            url_args=[user.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.User.objects.get(id=user.id).full_name, data['full_name'])

    def test_update_user_fail(self):
        user = create_user(seeds.USER_DATA)
        data = {
            'password': 'p',
        }
        response = request(
            'PATCH',
            'user-detail',
            data,
            http_authorization=get_http_authorization(
                seeds.USER_DATA['username'],
                seeds.USER_DATA['password']
            ),
            url_args=[user.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class BankAccountTransferDestinationTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_bank_account_transfer_destination_model_string_representation(self):
        bank_account_transfer_destination = models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )
        self.assertEqual(
            str(bank_account_transfer_destination),
            '{}: {} - {}'.format(
                bank_account_transfer_destination.bank_name,
                bank_account_transfer_destination.bank_code_number,
                bank_account_transfer_destination.bank_account_number
            )
        )

    def test_bank_account_transfer_destination_list_success(self):
        response = request(
            'GET',
            'bank-account-transfer-destination-list',
            http_authorization=self.superuser_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_bank_account_transfer_destination_detail_success(self):
        bank_account_transfer_destination = models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )
        response = request(
            'GET',
            'bank-account-transfer-destination-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[bank_account_transfer_destination.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_bank_account_transfer_destination_success(self):
        data = seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        response = request(
            'POST',
            'bank-account-transfer-destination-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.BankAccountTransferDestination.objects.count(), 1)
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=response.data['id']
            ).bank_code_number,
            data['bank_code_number']
        )
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=response.data['id']
            ).bank_account_number,
            data['bank_account_number']
        )

    def test_create_bank_account_transfer_destination_fail(self):
        response = request(
            'POST',
            'bank-account-transfer-destination-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.BankAccountTransferDestination.objects.count(), 0)

    def test_update_bank_account_transfer_destination_success(self):
        bank_account_transfer_destination = models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )
        data = {
            'bank_code_number': '333',
            'bank_account_number': 'Another Dummy Bank Account Number',
        }
        response = request(
            'PATCH',
            'bank-account-transfer-destination-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[bank_account_transfer_destination.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=bank_account_transfer_destination.id
            ).bank_code_number,
            data['bank_code_number']
        )
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=bank_account_transfer_destination.id
            ).bank_account_number,
            data['bank_account_number']
        )
        data = seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        response = request(
            'PUT',
            'bank-account-transfer-destination-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[bank_account_transfer_destination.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=bank_account_transfer_destination.id
            ).bank_code_number,
            data['bank_code_number']
        )
        self.assertEqual(
            models.BankAccountTransferDestination.objects.get(
                id=bank_account_transfer_destination.id
            ).bank_account_number,
            data['bank_account_number']
        )

    def test_update_bank_account_transfer_destination_fail(self):
        bank_account_transfer_destination = models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )
        data = {
            'bank_code_number': '',
            'bank_account_number': '',
        }
        response = request(
            'PATCH',
            'bank-account-transfer-destination-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[bank_account_transfer_destination.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class CategoryTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_category_model_string_representation(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.assertEqual(str(category), seeds.CATEGORY_DATA['name'])

    def test_category_list_success(self):
        response = request(
            'GET',
            'category-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_category_detail_success(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        response = request(
            'GET',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_category_success(self):
        data = seeds.CATEGORY_DATA
        response = request(
            'POST',
            'category-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Category.objects.count(), 1)
        self.assertEqual(models.Category.objects.get(id=response.data['id']).name, data['name'])

    def test_create_category_fail(self):
        response = request(
            'POST',
            'category-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Category.objects.count(), 0)

    def test_delete_category_success(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        response = request(
            'DELETE',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.Category.objects.count(), 0)

    def test_delete_category_fail(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA, category=category
        ))
        models.Product.objects.create(**dict(seeds.PRODUCT_DATA, subcategory=subcategory))
        response = request(
            'DELETE',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(models.Category.objects.count(), 1)


class SubcategoryTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.category = models.Category.objects.create(**seeds.CATEGORY_DATA)

    def test_subcategory_model_string_representation(self):
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=self.category
        ))
        self.assertEqual(str(subcategory), seeds.SUBCATEGORY_DATA['name'])

    def test_subcategory_list_success(self):
        response = request(
            'GET',
            'subcategory-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_subcategory_detail_success(self):
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=self.category
        ))
        response = request(
            'GET',
            'subcategory-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[subcategory.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_subcategory_success(self):
        data = dict(seeds.SUBCATEGORY_DATA, category=self.category.id)
        response = request(
            'POST',
            'subcategory-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Subcategory.objects.count(), 1)
        self.assertEqual(models.Subcategory.objects.get(id=response.data['id']).name, data['name'])

    def test_create_subcategory_fail(self):
        response = request(
            'POST',
            'subcategory-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Subcategory.objects.count(), 0)

    def test_delete_subcategory_success(self):
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=self.category
        ))
        response = request(
            'DELETE',
            'subcategory-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[subcategory.id]
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.Subcategory.objects.count(), 0)

    def test_delete_subcategory_fail(self):
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=self.category
        ))
        models.Product.objects.create(**dict(seeds.PRODUCT_DATA, subcategory=subcategory))
        response = request(
            'DELETE',
            'subcategory-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[subcategory.id]
        )
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(models.Subcategory.objects.count(), 1)


class ProductTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=self.category
        ))

    def test_product_model_string_representation(self):
        product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA, subcategory=self.subcategory
        ))
        self.assertEqual(len(str(product)), 6)

    def test_product_list_success(self):
        response = request(
            'GET',
            'product-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_product_detail_success(self):
        product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA, subcategory=self.subcategory
        ))
        response = request(
            'GET',
            'product-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[product.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_product_success(self):
        data = seeds.PRODUCT_DATA
        data['subcategory']= self.subcategory.id
        data['unit'] = 'kg'
        
        response = request(
            'POST',
            'product-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Product.objects.count(), 1)
        self.assertEqual(models.Product.objects.get(id=response.data['id']).name, data['name'])
        self.assertEqual(models.Product.objects.get(id=response.data['id']).unit, data['unit'])
        self.assertEqual(models.Product.objects.get(id=response.data['id']).preorder, data['preorder'])
        self.assertEqual(models.Product.objects.get(id=response.data['id']).preorder_duration, data['preorder_duration'])

    def test_create_product_fail(self):
        data = dict(seeds.PRODUCT_DATA, subcategory=self.subcategory.id)
        data['name'] = None
        data['unit'] = None
        response = request(
            'POST',
            'product-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Product.objects.count(), 0)

    def test_update_product_success(self):
        product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA, subcategory=self.subcategory
        ))
        data = {
            'name': 'Dummy',
            'price':'4000',
            'modal':'2000',
            'unit': 'gram',
            'is_hampers': True,
            'hampers_price': '500',
        }
        response = request(
            'PATCH',
            'product-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[product.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Product.objects.get(id=product.id).profit,2000)
        self.assertEqual(models.Product.objects.get(id=product.id).name, data['name'])
        self.assertEqual(models.Product.objects.get(id=product.id).unit, data['unit'])
        self.assertEqual(models.Product.objects.get(id=product.id).is_hampers, data['is_hampers'])
        data = dict(seeds.PRODUCT_DATA, subcategory=self.subcategory.id)
        response = request(
            'PUT',
            'product-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[product.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Product.objects.get(id=product.id).name, data['name'])

    def test_update_product_fail(self):
        product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA, subcategory=self.subcategory
        ))
        data = {
            'name': '',
            'unit': ''
        }
        response = request(
            'PATCH',
            'product-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[product.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class ShoppingCartTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.user = create_user(seeds.USER_DATA)

    def test_shopping_cart_model_string_representation(self):
        shopping_cart = models.ShoppingCart.objects.get(user=self.user)
        self.assertEqual(str(shopping_cart), self.user.username)

    def test_shopping_cart_list_success(self):
        response = request(
            'GET',
            'shopping-cart-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_shopping_cart_detail_success(self):
        response = request(
            'GET',
            'shopping-cart-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=['self']
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CartItemTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.user = create_user(seeds.USER_DATA)
        self.category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.subcategory = models.Subcategory.objects.create(**dict(
            seeds.CATEGORY_DATA,
            category=self.category
        ))
        self.product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA,
            subcategory=self.subcategory
        ))
        self.shopping_cart = models.ShoppingCart.objects.get(user=self.user)

    def test_cart_item_model_string_representation(self):
        product_code = self.product.code
        cart_item = models.CartItem.objects.create(
            shopping_cart=self.shopping_cart,
            product=self.product
        )
        self.assertEqual(str(cart_item), product_code)


class TransactionTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        self.user = create_user(seeds.USER_DATA)
        self.user_http_authorization = get_http_authorization(
            seeds.USER_DATA['username'],
            seeds.USER_DATA['password']
        )
        self.batch = models.Batch.objects.create(**seeds.BATCH_DATA)


    def test_transaction_model_string_representation(self):
        transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA, user=self.user
        ))
        self.assertEqual(len(str(transaction)), 8)

    def test_transaction_list_success(self):
        response = request(
            'GET',
            'transaction-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = request(
            'GET',
            'transaction-list',
            http_authorization=self.user_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_transaction_detail_success(self):
        transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA, user=self.user
        ))
        response = request(
            'GET',
            'transaction-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[transaction.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_transaction_success(self):
        transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA, user=self.user
        ))
        data = {
            'transaction_status': '006',
        }
        response = request(
            'PATCH',
            'transaction-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[transaction.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            models.Transaction.objects.get(id=transaction.id).transaction_status,
            data['transaction_status']
        )

    def test_update_transaction_fail(self):
        transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA, user=self.user
        ))
        transaction.transaction_status = '005'
        transaction.save()
        data = {
            'transaction_status': '006',
        }
        response = request(
            'PATCH',
            'transaction-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[transaction.id]
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        transaction.transaction_status = '003'
        transaction.save()
        data = {
            'transaction_status': '007',
        }
        response = request(
            'PATCH',
            'transaction-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[transaction.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TransactionItemTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        models.Batch.objects.create(**seeds.BATCH_DATA)
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        self.user = create_user(seeds.USER_DATA)
        self.category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.subcategory = models.Subcategory.objects.create(**dict(
            seeds.CATEGORY_DATA,
            category=self.category
        ))
        self.product = models.Product.objects.create(**dict(
            seeds.PRODUCT_DATA,
            subcategory=self.subcategory
        ))
        self.transaction = models.Transaction.objects.create(**dict(
            seeds.TRANSACTION_DATA,
            user=self.user
        ))


    def test_transaction_item_model_string_representation(self):
        transaction_item = models.TransactionItem.objects.create(**dict(
            seeds.TRANSACTION_ITEM_DATA,
            transaction=self.transaction,
            product=self.product
        ))
        self.assertEqual(str(transaction_item), self.product.name)

    def test_transaction_item_model_profit(self):
        transaction_item = models.TransactionItem.objects.create(**dict(
            seeds.TRANSACTION_ITEM_DATA,
            transaction=self.transaction,
            product=self.product
        ))
        self.assertEqual(transaction_item.profit, 1000)


class ProgramTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_program_model_string_representation(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        self.assertEqual(len(str(program)), 6)

    def test_program_list_success(self):
        response = request(
            'GET',
            'program-list',
            http_authorization=self.superuser_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_program_detail_success(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        response = request(
            'GET',
            'program-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_program_success(self):
        data = seeds.PROGRAM_DATA
        response = request(
            'POST',
            'program-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Program.objects.count(), 1)
        self.assertEqual(models.Program.objects.get(id=response.data['id']).name, data['name'])

    def test_create_program_fail(self):
        response = request(
            'POST',
            'program-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Program.objects.count(), 0)

    def test_update_program_success(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        data = {
            'name': 'Dummy',
        }
        response = request(
            'PATCH',
            'program-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Program.objects.get(id=program.id).name, data['name'])
        data = seeds.PROGRAM_DATA
        response = request(
            'PUT',
            'program-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Program.objects.get(id=program.id).name, data['name'])

    def test_update_program_fail(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        data = {
            'end_date_time': '2020-01-01T00:00:00+07:00',
        }
        response = request(
            'PATCH',
            'program-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ProgramDonationTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.user = create_user(seeds.USER_DATA)
        self.user_http_authorization = get_http_authorization(
            seeds.USER_DATA['username'],
            seeds.USER_DATA['password']
        )
        self.program = models.Program.objects.create(**seeds.PROGRAM_DATA)
        models.BankAccountTransferDestination.objects.create(
            **seeds.BANK_ACCOUNT_TRANSFER_DESTINATION
        )

    def test_program_donation_model_string_representation(self):
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=self.program
        ))
        self.assertEqual(len(str(program_donation)), 6)
    
    def test_program_donation_list_success(self):
        response = request(
            'GET',
            'program-donation-list',
            http_authorization=self.superuser_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
    

    def test_program_donation_csh_list_success(self):
        response = request(
            'GET',
            'program-donation-csh-list',
            http_authorization=self.superuser_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_program_donation_gds_list_success(self):
        response = request(
            'GET',
            'program-donation-gds-list',
            http_authorization=self.user_http_authorization,
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_program_donation_detail_success(self):
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=self.program
        ))
        response = request(
            'GET',
            'program-donation-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[program_donation.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_program_donation_success(self):
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=self.program
        ))
        data = {
            'donation_status': '002',
        }
        response = request(
            'PATCH',
            'program-donation-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[program_donation.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            models.ProgramDonation.objects.get(id=program_donation.id).donation_status,
            data['donation_status']
        )

    def test_update_program_donation_fail(self):
        program_donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=self.program
        ))
        program_donation.donation_status = '002'
        program_donation.save()
        data = {
            'donation_status': '001',
        }
        response = request(
            'PATCH',
            'program-donation-detail',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[program_donation.id]
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    def test_delete_by_program(self):
        program = models.Program.objects.create(**seeds.PROGRAM_DATA )
        donation = models.ProgramDonation.objects.create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=self.user,
            program=program))
        response = request(
            'DELETE',
            'donation-by-program',
            http_authorization=self.superuser_http_authorization,
            url_args=[program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.ProgramDonation.objects.count(), 0)
        


class ChoicesViewsTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_payment_method_choices_success(self):
        response = request(
            'GET',
            'payment-method-choice',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_transaction_status_choices_success(self):
        response = request(
            'GET',
            'transaction-status-choice',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_donation_status_choices_success(self):
        response = request(
            'GET',
            'donation-status-choice',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AppConfigTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_app_config_model_string_representation(self):
        app_config = models.AppConfig.objects.create()
        self.assertTrue(len(str(app_config)) > 0)

    def test_app_config_detail_success(self):
        models.AppConfig.objects.create()
        response = request(
            'GET',
            'app-config-detail',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_app_config_success(self):
        models.AppConfig.objects.create()
        data = {
            'send_sms': True,
        }
        response = request(
            'PATCH',
            'app-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.AppConfig.objects.get().send_sms, data['send_sms'])

    def test_update_app_config_fail(self):
        models.AppConfig.objects.create()
        data = {
            'send_sms': None,
        }
        response = request(
            'PATCH',
            'app-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class HelpContactConfigTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_help_account_config_model_string_representation(self):
        help_account_config = models.HelpContactConfig.objects.create(
            **seeds.HELP_CONTACT_CONFIG_DATA
        )
        self.assertTrue(len(str(help_account_config)) > 0)

    def test_help_account_config_detail_success(self):
        models.HelpContactConfig.objects.create(**seeds.HELP_CONTACT_CONFIG_DATA)
        response = request(
            'GET',
            'help-contact-config-detail',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_help_account_config_success(self):
        models.HelpContactConfig.objects.create(**seeds.HELP_CONTACT_CONFIG_DATA)
        data = {
            'email': 'another.dummy@example.com',
        }
        response = request(
            'PATCH',
            'help-contact-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.HelpContactConfig.objects.get().email, data['email'])

    def test_update_help_account_config_fail(self):
        models.HelpContactConfig.objects.create(**seeds.HELP_CONTACT_CONFIG_DATA)
        data = {
            'email': '',
        }
        response = request(
            'PATCH',
            'help-contact-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ShipmentConfigTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_shipment_config_model_string_representation(self):
        shipment_config = models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        self.assertTrue(len(str(shipment_config)) > 0)

    def test_shipment_config_detail_success(self):
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        response = request(
            'GET',
            'shipment-config-detail',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_shipment_config_success(self):
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        data = {
            'hamlet': '001',
        }
        response = request(
            'PATCH',
            'shipment-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.ShipmentConfig.objects.get().hamlet, data['hamlet'])

    def test_update_shipment_config_fail(self):
        models.ShipmentConfig.objects.create(**seeds.SHIPMENT_CONFIG_DATA)
        data = {
            'hamlet': '1',
        }
        response = request(
            'PATCH',
            'shipment-config-detail',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class CategoryTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_category_model_string_representation(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        self.assertEqual(str(category), seeds.CATEGORY_DATA['name'])

    def test_category_list_success(self):
        response = request(
            'GET',
            'category-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_category_detail_success(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        response = request(
            'GET',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_category_success(self):
        data = seeds.CATEGORY_DATA
        response = request(
            'POST',
            'category-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Category.objects.count(), 1)
        self.assertEqual(models.Category.objects.get(id=response.data['id']).name, data['name'])

    def test_create_category_fail(self):
        response = request(
            'POST',
            'category-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Category.objects.count(), 0)

    def test_delete_category_success(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        response = request(
            'DELETE',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(models.Category.objects.count(), 0)

    def test_delete_category_fail(self):
        category = models.Category.objects.create(**seeds.CATEGORY_DATA)
        subcategory = models.Subcategory.objects.create(**dict(
            seeds.SUBCATEGORY_DATA, category=category
        ))
        models.Product.objects.create(**dict(seeds.PRODUCT_DATA, subcategory=subcategory))
        response = request(
            'DELETE',
            'category-detail',
            http_authorization=self.superuser_http_authorization,
            url_args=[category.id]
        )
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)
        self.assertEqual(models.Category.objects.count(), 1)

class BatchTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )

    def test_batch_model_string_representation(self):
        batch = models.Batch.objects.create(**seeds.BATCH_DATA)
        self.assertEqual(str(batch), seeds.BATCH_DATA['batch_name'])

    def test_batch_list_success(self):
        response = request(
            'GET',
            'batch-list',
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_batch_success(self):
        data = seeds.BATCH_DATA
        response = request(
            'POST',
            'batch-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.Batch.objects.count(), 1)
        self.assertEqual(models.Batch.objects.get(
            id=response.data['id']).batch_name, data['batch_name']
        )

    def test_create_batch_fail(self):
        data = {
            'batch_name': 'Batch 1',
            'start_date': '2020-10-15',
            'end_date': '',
            'shipping_cost': '60000',
        }
        response = request(
            'POST',
            'batch-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Batch.objects.count(), 0)

    def test_create_batch_wrong_date(self):
        data = {
            'batch_name': 'Batch 1',
            'start_date': '2020-10-15',
            'end_date': '2020-10-8',
            'shipping_cost': '60000',
        }
        response = request(
            'POST',
            'batch-list',
            data,
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(models.Batch.objects.count(), 0)

    def test_edit_batch_success(self):
        batch = models.Batch.objects.create(**seeds.BATCH_DATA)
        data = seeds.BATCH_DATA
        data['shipping_cost'] = 30000
        response = request(
            'PATCH',
            'batch-detail',
            data,
            url_args=[batch.id],
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(models.Batch.objects.get(id=batch.id).shipping_cost, data['shipping_cost'])

    def test_edit_batch_fail(self):
        batch = models.Batch.objects.create(**seeds.BATCH_DATA)
        data = seeds.BATCH_DATA
        data['shipping_cost'] = 30000
        response = request(
            'PATCH',
            'batch-detail',
            data,
            url_args=[0],
            http_authorization=self.superuser_http_authorization
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(models.Batch.objects.get(id=batch.id).shipping_cost, 60000)

class ProgramProgressTest(rest_framework_test.APITestCase):
    def setUp(self):
        self.superuser = models.User.objects.create_superuser(**seeds.SUPERUSER_DATA)
        self.superuser_http_authorization = get_http_authorization(
            seeds.SUPERUSER_DATA['username'],
            seeds.SUPERUSER_DATA['password']
        )
        self.program = models.Program.objects.create(**seeds.PROGRAM_DATA)

    def test_program_progress_list_success(self):
        response = request(
            'GET',
            'program-progress-list',
            http_authorization=self.superuser_http_authorization,
            url_args=[self.program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_program_progress_success(self):
        data = seeds.PROGRAM_PROGRESS_DATA
        response = request(
            'POST',
            'program-progress-list',
            data,
            http_authorization=self.superuser_http_authorization,
            url_args=[self.program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(models.ProgramProgress.objects.count(), 1)
        self.assertEqual(models.ProgramProgress.objects.get(
            id=response.data['id']).description, data['description']
        )

    def test_create_program_progress_fail(self):
        response = request(
            'POST',
            'program-progress-list',
            http_authorization=self.superuser_http_authorization,
            url_args=[self.program.id]
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.ProgramProgress.objects.count(), 0)
