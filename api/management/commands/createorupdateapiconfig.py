import os

import yaml
from django.core.management import base

from api import models


class Command(base.BaseCommand):
    def handle(self, *args, **options):
        with open(os.environ.get('API_CONFIG_PATH', 'api_config.yaml')) as file:
            api_config = yaml.load(file, Loader=yaml.FullLoader)
            config_key_to_model_class_map = {
                'app_config': models.AppConfig,
                'help_contact_config': models.HelpContactConfig,
                'shipment_config': models.ShipmentConfig,
            }
            for key, model_class in config_key_to_model_class_map.items():
                model_class.objects.update_or_create(defaults=api_config[key])
