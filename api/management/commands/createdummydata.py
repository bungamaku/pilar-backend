from django.core.management import base
from api import models
from api import seeds

class Command(base.BaseCommand):
    def handle(self, *args, **kwargs):
        user, status = models.User.objects.get_or_create(**seeds.USER_DATA)
        bank_account_transfer_destination, status = models.BankAccountTransferDestination.objects.get_or_create(**seeds.BANK_ACCOUNT_TRANSFER_DESTINATION)

        category, status = models.Category.objects.get_or_create(**seeds.CATEGORY_DATA)
        subcategory, status = models.Subcategory.objects.get_or_create(**dict(
            seeds.SUBCATEGORY_DATA,
            category=category
        ))
        product, status = models.Product.objects.get_or_create(**dict(seeds.PRODUCT_DATA, subcategory=subcategory))
        transaction, status = models.Transaction.objects.get_or_create(**dict(
            seeds.TRANSACTION_DATA, user=user
        ))
        transaction_item, status = models.TransactionItem.objects.get_or_create(**dict(
            seeds.TRANSACTION_ITEM_DATA,
            transaction=transaction,
            product=product
        ))

        program, status = models.Program.objects.get_or_create(**seeds.PROGRAM_DATA)
        program_donation_pck, status = models.ProgramDonation.objects.get_or_create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=user,
            program=program
        ))
        program_donation_dlv, status = models.ProgramDonation.objects.get_or_create(**dict(
            seeds.PROGRAM_DONATION_CASH_DATA,
            user=user,
            program=program
        ))

        batch, status = models.Batch.objects.get_or_create(**seeds.BATCH_DATA)
