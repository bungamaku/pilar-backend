from django import apps


class ApiConfig(apps.AppConfig):
    name = 'api'

    def ready(self):
        from api import signals # pylint: disable=import-outside-toplevel,unused-import
