import datetime
from datetime import timedelta
import jwt
import shortuuid
from jwt import exceptions as jwt_exceptions
from django import conf
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions as rest_framework_exceptions

from home_industry import utils
from api import models


def generate_bearer_token(user):
    encoded_jwt = jwt.encode(
        {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=1),
            'username': user.username,
        },
        conf.settings.SECRET_KEY,
        algorithm='HS256'
    ).decode('utf-8')
    return encoded_jwt


def generate_code():
    alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    code = shortuuid.ShortUUID(alphabet=alphabet).random(length=6)
    return code


def generate_donation_number():
    alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    donation_number = shortuuid.ShortUUID(alphabet=alphabet).random(length=6)
    return donation_number


def generate_otp():
    alphabet = '0123456789'
    otp = shortuuid.ShortUUID(alphabet=alphabet).random(length=6)
    return otp


def generate_transaction_number():
    alphabet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'
    transaction_number = shortuuid.ShortUUID(alphabet=alphabet).random(length=8)
    return transaction_number


def get_shipping_costs(user):
    shipment_config = utils.get_shipment_config()
    if user.sub_district.lower() == shipment_config.sub_district.lower():
        if user.urban_village.lower() == shipment_config.urban_village.lower():
            if user.hamlet == shipment_config.hamlet:
                shipping_costs = shipment_config.same_hamlet_costs
            else:
                shipping_costs = (
                    shipment_config.same_urban_village_different_hamlet_costs
                )
        else:
            shipping_costs = (
                shipment_config.same_sub_district_different_urban_village_costs
            )
    else:
        shipping_costs = None
    return shipping_costs


def get_upload_file_path(instance, filename):
    directory = type(instance).__name__.lower()
    file_name = '{}_{}'.format(shortuuid.uuid(), filename)
    return 'uploads/{}/{}'.format(directory, file_name)


def get_username_from_bearer_token(http_authorization):
    if http_authorization is None:
        raise rest_framework_exceptions.NotAuthenticated()
    bearer, _sep, encoded_jwt = http_authorization.partition(' ')
    if bearer != 'Bearer':
        raise rest_framework_exceptions.AuthenticationFailed(_(
            'Invalid authorization header format.'
        ))
    try:
        decoded_jwt = jwt.decode(encoded_jwt, conf.settings.SECRET_KEY, algorithms=['HS256'])
    except (jwt_exceptions.DecodeError, jwt_exceptions.ExpiredSignatureError):
        raise rest_framework_exceptions.AuthenticationFailed(_('Invalid token.'))
    if (decoded_jwt.get('exp') is None) or (decoded_jwt.get('username') is None):
        raise rest_framework_exceptions.AuthenticationFailed(_('Invalid token.'))
    return decoded_jwt['username']


def map_choices(choices):
    return [{'code': choice[0], 'description': choice[1]} for choice in choices]


def return_transaction_items_to_product_stock(transaction_items):
    for transaction_item in transaction_items:
        if (transaction_item.product is not None) and (transaction_item.product.stock is not None):
            transaction_item.product.stock += transaction_item.quantity
            transaction_item.product.save()


def send_otp(phone_number, otp):
    message = _( # pylint: disable=no-member
        'Your Industri Pilar account authentication code is {otp}. '
        'Keep your authentication code SECRET.'
    ).format(otp=otp)
    utils.send_sms(phone_number, message)


def validate_product_stock(cart_items):
    for cart_item in cart_items:
        product = cart_item.product
        if (product.stock is not None) and (cart_item.quantity > product.stock):
            raise rest_framework_exceptions.ParseError(_(
                'Failed to checkout because the purchased quantity of certain items exceeds the '
                'available stock.'
            ))


def get_batch_transaction(transaction):
    today = timezone.now().date()
    transaction_batch = models.Batch.objects.filter(start_date__lte=today, end_date__gt=today).first()
    return transaction_batch

def get_transfer_destination(program_donation):
    bank_name = program_donation.user_bank_name
    bank_account_name = program_donation.user_bank_account_name
    transfer_destination = None
    for bank_destination in models.BankAccountTransferDestination.objects.all():
        if (bank_destination.bank_name == bank_name) and (bank_destination.bank_account_name == bank_account_name):
            transfer_destination = bank_destination
    return transfer_destination
