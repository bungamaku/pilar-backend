import collections
import datetime
import decimal
import io
import numbers
import operator

import xlsxwriter
from django import conf
from django.utils.translation import gettext_lazy as _

from api import filters, models


def donation_donation_number_with_hyperlink(worksheet, row, col, obj, cell_format):
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['PROGRAM_DONATION_PATH'],
            obj.id
        ),
        cell_format=cell_format,
        string=str(obj.donation_number)
    )


def product_code_with_hyperlink(worksheet, row, col, obj, cell_format):
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['PRODUCT_PATH'],
            obj.id
        ),
        cell_format=cell_format,
        string=str(obj.code)
    )


def product_sold(worksheet, row, col, obj, cell_format):
    worksheet.write_number(row, col, sum(
        transaction_item.quantity
        for transaction_item in models.TransactionItem.objects.filter(
            transaction__transaction_status='005',
            product=obj
        )
    ), cell_format=cell_format)


def program_code_with_hyperlink(worksheet, row, col, obj, cell_format):
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['PROGRAM_PATH'],
            obj.id
        ),
        cell_format=cell_format,
        string=str(obj.code)
    )


def program_donation_program_code_with_hyperlink(worksheet, row, col, obj, cell_format):
    if obj.program is None:
        return
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['PROGRAM_PATH'],
            obj.program.id
        ),
        cell_format=cell_format,
        string=str(obj.program.code)
    )


def program_total_donation_amount(worksheet, row, col, obj, cell_format):
    worksheet.write_number(row, col, sum(
        program_donation.amount for program_donation in obj.program_donations.filter(
            donation_status='002',
            donation_type='CSH'
        )
    ), cell_format=cell_format)


def transaction_item_transaction_transaction_number_with_hyperlink(worksheet, row, col, obj,
                                                                   cell_format):
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['TRANSACTION_PATH'],
            obj.transaction.id
        ),
        cell_format=cell_format,
        string=str(obj.transaction.transaction_number)
    )


def transaction_or_donation_user_username_with_hyperlink(worksheet, row, col, obj, cell_format):
    if obj.user is None:
        return
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['USER_PATH'],
            obj.user.id
        ),
        cell_format=cell_format,
        string=str(obj.user.username)
    )


def transaction_transaction_item_subtotal(worksheet, row, col, obj, cell_format):
    worksheet.write_number(row, col, sum(
        (transaction_item.product_price + transaction_item.hampers_price)
        * transaction_item.quantity for transaction_item in obj.transaction_items.all()
    ), cell_format=cell_format)


def transaction_transaction_number_with_hyperlink(worksheet, row, col, obj, cell_format):
    worksheet.write_url(
        row,
        col,
        '{}{}{}'.format(
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['URL'],
            conf.settings.HOME_INDUSTRY_ADMIN_SITE['TRANSACTION_PATH'],
            obj.id
        ),
        cell_format=cell_format,
        string=str(obj.transaction_number)
    )


def write_headers_to_worksheet(worksheet, headers, row=0, start_col=0, header_format=None, # pylint: disable=too-many-arguments
                               col_width=None):
    if col_width is not None:
        worksheet.set_column(0, len(headers) - 1, col_width)
    for col, header in enumerate(headers, start=start_col):
        worksheet.write_string(row, col, str(header), cell_format=header_format)


def write_queryset_data_to_worksheet(worksheet, queryset, fields, start_col=0, start_row=0, # pylint: disable=too-many-arguments
                                     header_format=None, data_format=None, col_width=None):
    fields = collections.OrderedDict(fields)
    write_headers_to_worksheet(
        worksheet,
        fields.values(),
        row=start_row,
        start_col=start_col,
        header_format=header_format,
        col_width=col_width
    )
    row = start_row
    for row, obj in enumerate(queryset, start=row + 1):
        for col, field_name in enumerate(fields.keys(), start=start_col):
            cell_format = data_format.get(field_name) if data_format is not None else None
            if callable(field_name):
                field_name(worksheet, row, col, obj, cell_format)
                continue
            value = operator.attrgetter(field_name)(obj)
            if value is None:
                continue
            value = value() if callable(value) else value
            if isinstance(value, bool):
                worksheet.write_boolean(row, col, value, cell_format=cell_format)
            elif isinstance(value, datetime.datetime):
                worksheet.write_datetime(row, col, value, cell_format=cell_format)
            elif isinstance(value, (decimal.Decimal, numbers.Real)):
                worksheet.write_number(row, col, value, cell_format=cell_format)
            else:
                worksheet.write_string(row, col, str(value), cell_format=cell_format)
    return row


def create_transaction_report(filter_params): # pylint: disable=too-many-locals
    buffer = io.BytesIO()
    workbook = xlsxwriter.Workbook(
        buffer,
        {
            'default_date_format': 'yyyy/mm/dd hh:mm',
            'remove_timezone': True,
        }
    )
    header_format = workbook.add_format(
        {
            'align': 'center',
            'bg_color': '#408EBA',
            'bold': 1,
            'font_color': '#FFFFFF',
            'font_size': 14,
        }
    )
    money_format = workbook.add_format({'num_format': '#,##0'})
    transaction_worksheet = workbook.add_worksheet(str(_('Transactions')))
    transaction_fields = [
        (transaction_transaction_number_with_hyperlink, _('Transaction Number')),
        (transaction_or_donation_user_username_with_hyperlink, _('User Username')),
        ('user_full_name', _('User Full Name')),
        ('user_phone_number', _('User Phone Number')),
        ('shipping_address', _('Shpping Address')),
        ('shipping_neighborhood', _('Shipping Neighborhood')),
        ('shipping_hamlet', _('Shipping Hamlet')),
        ('shipping_urban_village', _('Shipping Urban Village')),
        ('shipping_sub_district', _('Shipping Sub-District')),
        (transaction_transaction_item_subtotal, _('Transaction Item Subtotal')),
        ('shipping_costs', _('Shipping Costs')),
        ('get_payment_method_display', _('Payment Method')),
        ('donation', _('Donation')),
        ('get_transaction_status_display', _('Transaction Status')),
        ('user_bank_name', _('User Bank Name')),
        ('user_bank_account_name', _('User Bank Account Name')),
        ('transfer_destination_bank_name', _('Transfer Destination Bank Name')),
        ('transfer_destination_bank_account_name', _('Transfer Destination Bank Account Name')),
        (
            'transfer_destination_bank_account_number',
            _('Transfer Destination Bank Account Number'),
        ),
        ('created_at', _('Created at')),
        ('updated_at', _('Updated at')),
    ]
    transactions = filters.ReportTransactionFilter(filter_params).qs
    write_queryset_data_to_worksheet(
        transaction_worksheet,
        transactions,
        transaction_fields,
        header_format=header_format,
        data_format={
            transaction_transaction_item_subtotal: money_format,
            'shipping_costs': money_format,
            'donation': money_format,
        },
        col_width=32
    )
    transaction_item_worksheet = workbook.add_worksheet(str(_('Transaction Items')))
    transaction_item_fields = [
        (transaction_item_transaction_transaction_number_with_hyperlink, _('Transaction Number')),
        ('product_name', _('Product Name')),
        ('product_price', _('Product Price')),
        ('hampers_price', _('Hampers Price')),
        ('hampers_messages', _('Hampers Messages')),
        ('quantity', _('Quantity')),
    ]
    write_queryset_data_to_worksheet(
        transaction_item_worksheet,
        models.TransactionItem.objects.filter(transaction__in=transactions),
        transaction_item_fields,
        header_format=header_format,
        data_format={
            'product_price': money_format,
            'hampers_price': money_format
        },
        col_width=32
    )
    product_order_summary_worksheet = workbook.add_worksheet(str(_('Product Order Summary')))
    product_order_summary_fields = [
        (product_code_with_hyperlink, _('Product Code')),
        ('name', _('Product Name')),
        (product_sold, _('Sold')),
    ]
    last_row = write_queryset_data_to_worksheet(
        product_order_summary_worksheet,
        models.Product.objects.all(),
        product_order_summary_fields,
        header_format=header_format,
        col_width=32
    )
    product_order_summary_worksheet.write_string(last_row + 1, 0, str(_(
        'NB: This product order summary only shows products that have not been deleted. '
        'It also not affected by date filtering.'
    )))
    summary_worksheet = workbook.add_worksheet(str(_('Transaction Summary')))
    summary_worksheet.set_column(0, 1, 32)
    summary_worksheet.write_string(0, 0, str(_('Revenue (Transaction)')), workbook.add_format(
        {
            'bg_color': '#94C380',
            'bold': 1,
            'font_size': 14,
        }
    ))
    transactions = transactions.filter(transaction_status='005')
    transaction_revenue = sum(
        transaction_item.product_price * transaction_item.quantity
        for transaction_item in models.TransactionItem.objects.filter(transaction__in=transactions)
    )
    summary_worksheet.write_number(0, 1, transaction_revenue, money_format)
    summary_worksheet.write_string(1, 0, str(_('Donation (Transaction)')), workbook.add_format(
        {
            'bg_color': '#3AA757',
            'bold': 1,
            'font_size': 14,
        }
    ))
    transaction_donation = sum(
        transaction_item.donation for transaction_item in transactions
    )
    summary_worksheet.write_number(1, 1, transaction_donation, money_format)
    workbook.close()
    return buffer


def create_program_donation_report_csh(filter_params):
    buffer = io.BytesIO()
    workbook = xlsxwriter.Workbook(
        buffer,
        {
            'default_date_format': 'yyyy/mm/dd hh:mm',
            'remove_timezone': True,
        }
    )
    header_format = workbook.add_format(
        {
            'align': 'center',
            'bg_color': '#408EBA',
            'bold': 1,
            'font_color': '#FFFFFF',
            'font_size': 14,
        }
    )
    money_format = workbook.add_format({'num_format': '#,##0'})
    program_donation_worksheet = workbook.add_worksheet(str(_('Program Donations of CSH')))
    program_donation_fields = [
        (donation_donation_number_with_hyperlink, _('Donation Number')),
        (transaction_or_donation_user_username_with_hyperlink, _('User Username')),
        (program_donation_program_code_with_hyperlink, _('Program Code')),
        ('user_full_name', _('User Full Name')),
        ('user_phone_number', _('User Phone Number')),
        ('program_name', _('Program Name')),
        ('amount', _('Amount')),
        ('get_donation_status_display', _('Donation Status')),
        ('user_bank_name', _('User Bank Name')),
        ('user_bank_account_name', _('User Bank Account Name')),
        ('transfer_destination_bank_name', _('Transfer Destination Bank Name')),
        ('transfer_destination_bank_account_name', _('Transfer Destination Bank Account Name')),
        (
            'transfer_destination_bank_account_number',
            _('Transfer Destination Bank Account Number'),
        ),
        ('created_at', _('Created at')),
        ('updated_at', _('Updated at')),
    ]
    write_queryset_data_to_worksheet(
        program_donation_worksheet,
        models.ProgramDonation.objects.filter(donation_type='CSH'),
        program_donation_fields,
        header_format=header_format,
        data_format={'amount': money_format},
        col_width=32
    )
    program_summary_worksheet = workbook.add_worksheet(str(_('Program Summary')))
    program_summary_fields = [
        (program_code_with_hyperlink, _('Program Code')),
        ('name', _('Program Name')),
        ('open_donation', _('Open for Donation')),
        (program_total_donation_amount, _('Total Donation Amount')),
    ]
    last_row = write_queryset_data_to_worksheet(
        program_summary_worksheet,
        models.Program.objects.all(),
        program_summary_fields,
        header_format=header_format,
        data_format={program_total_donation_amount: money_format},
        col_width=32
    )
    program_summary_worksheet.write_string(last_row + 1, 0, str(_(
        'NB: This program summary only shows programs that have not been deleted. '
        'It also not affected by date filtering.'
    )))
    workbook.close()
    return buffer

def create_program_donation_report_gds(filter_params):
    buffer = io.BytesIO()
    workbook = xlsxwriter.Workbook(
        buffer,
        {
            'default_date_format': 'yyyy/mm/dd hh:mm',
            'remove_timezone': True,
        }
    )
    header_format = workbook.add_format(
        {
            'align': 'center',
            'bg_color': '#408EBA',
            'bold': 1,
            'font_color': '#FFFFFF',
            'font_size': 14,
        }
    )
    money_format = workbook.add_format({'num_format': '#,##0'})
    program_donation_worksheet = workbook.add_worksheet(str(_('Program Donations of GDS')))
    program_donation_fields = [
        (donation_donation_number_with_hyperlink, _('Donation Number')),
        (transaction_or_donation_user_username_with_hyperlink, _('User Username')),
        (program_donation_program_code_with_hyperlink, _('Program Code')),
        ('user_full_name', _('User Full Name')),
        ('user_phone_number', _('User Phone Number')),
        ('program_name', _('Program Name')),
        ('get_donation_status_display', _('Donation Status')),
        ('goods_quantity', _('Goods Quantity')),
        ('goods_description', _('Goods Description')),
        ('delivery_method', _('Delivery Method')),
        ('delivery_address', _('Delivery Address')),
        ('created_at', _('Created at')),
        ('updated_at', _('Updated at'))

    ]
    write_queryset_data_to_worksheet(
        program_donation_worksheet,
        models.ProgramDonation.objects.filter(donation_type='GDS'),
        program_donation_fields,
        header_format=header_format,
        data_format={'amount': money_format},
        col_width=32
    )
    program_summary_worksheet = workbook.add_worksheet(str(_('Program Summary')))
    program_summary_fields = [
        (program_code_with_hyperlink, _('Program Code')),
        ('name', _('Program Name')),
        ('open_donation', _('Open for Donation')),
        (program_total_donation_amount, _('Total Donation Amount')),
    ]
    last_row = write_queryset_data_to_worksheet(
        program_summary_worksheet,
        models.Program.objects.all(),
        program_summary_fields,
        header_format=header_format,
        data_format={program_total_donation_amount: money_format},
        col_width=32
    )
    program_summary_worksheet.write_string(last_row + 1, 0, str(_(
        'NB: This program summary only shows programs that have not been deleted. '
        'It also not affected by date filtering.'
    )))
    workbook.close()
    return buffer
