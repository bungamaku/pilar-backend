import coreapi
import coreschema
from rest_framework import schemas


class AutoSchemaWithDateRange(schemas.AutoSchema):
    date_range_fields = []

    def get_filter_fields(self, path, method):
        filter_fields = super().get_filter_fields(path, method)
        for filter_field in filter_fields:
            if filter_field.name in self.date_range_fields:
                filter_fields.remove(filter_field)
        for date_range_field in self.date_range_fields:
            filter_fields += [
                coreapi.Field(
                    '{}_after'.format(date_range_field),
                    location='query',
                    required=False,
                    schema=coreschema.String()
                ),
                coreapi.Field(
                    '{}_before'.format(date_range_field),
                    location='query',
                    required=False,
                    schema=coreschema.String()
                ),
            ]
        return filter_fields


class ReportTransactionSchema(AutoSchemaWithDateRange):
    date_range_fields = ['created_at_date_range']
    def get_filter_fields(self, path, method):
        filter_fields = super().get_filter_fields(path, method)
        filter_fields += [
            coreapi.Field(
                'batch_name',
                location='query',
                required=False,
                schema=coreschema.String()
            )
        ]
        return filter_fields


class ReportProgramDonationSchema(AutoSchemaWithDateRange):
    date_range_fields = ['created_at_date_range']


class TransactionListSchema(AutoSchemaWithDateRange):
    date_range_fields = ['updated_at_date_range']


class ProgramDonationListSchema(AutoSchemaWithDateRange):
    date_range_fields = ['updated_at_date_range']


class BatchListSchema(AutoSchemaWithDateRange):
    date_range_fields = ['updated_at_date_range']