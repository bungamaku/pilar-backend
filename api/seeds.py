SUPERUSER_DATA = {
    'username': 'admin',
    'email': 'admin@example.com',
    'password': 'admin',
}

USER_DATA = {
    'username': 'dummyuser',
    'password': 'dummypassword',
    'full_name': 'Dummy Full Name',
    'phone_number': '+6285212345678',
    'address': 'Dummy Address',
    'neighborhood': '001',
    'hamlet': '001',
    'urban_village': 'Dummy Urban Village',
    'sub_district': 'Dummy Sub-District',
}

BANK_ACCOUNT_TRANSFER_DESTINATION = {
    'bank_name': 'Dummy Bank Name',
    'bank_code_number': '011',
    'bank_account_number': '123456789',
    'bank_account_name': 'Dummy Bank Account Name',
}

CATEGORY_DATA = {
    'name': 'Dummy Category',
}

SUBCATEGORY_DATA = {
    'name': 'Dummy Subcategory',
}

PRODUCT_DATA = {
    'name': 'Dummy Product',
    'description': 'Dummy description.',
    'price': '2000',
    'stock': 10,
    'modal': '1000',
    'unit': 'kg',
    'is_hampers': True,
    'hampers_price': '500',
    'preorder' : True,
    'preorder_duration' : 5,
}

TRANSACTION_DATA = {
    'payment_method': 'TRF',
    'donation': '1000',
    'transaction_status': '001',
}

TRANSACTION_ITEM_DATA = {
    'profit' : 1000,
    'quantity': 1,
}

PROGRAM_DATA = {
    'name': 'Dummy Program',
    'description': 'Dummy description.',
    'start_date_time': '2020-01-01T00:00:00+07:00',
    'end_date_time': '2020-02-02T00:00:00+07:00',
    'location': 'Dummy Location',
    'speaker': 'Dummy Speaker',
    'link': 'https://example.com',
}

PROGRAM_PROGRESS_DATA = {
    'date': '2020-10-26',
    'description': 'Progress update 1'
}

PROGRAM_DONATION_CASH_DATA = {
    'amount': '1000',
    'user_bank_name': 'Dummy Bank Name',
    'user_bank_account_name': 'Dummy Bank Account Name',
}

PROGRAM_DONATION_GOODS_DATA = {
    'goods_quantity': '10',
    'goods_description': 'Dummy Goods',
    'delivery_address': 'Dummy Address',
    'delivery_method': 'PCK',
}

PROGRAM_DONATION_GOODS_DATA_DLV = {
    'goods_quantity': '10',
    'goods_description': 'Dummy Goods',
    'delivery_address': None,
    'delivery_method': 'DLV',
}


HELP_CONTACT_CONFIG_DATA = {
    'email': 'dummy@example.com',
}

SHIPMENT_CONFIG_DATA = {
    'hamlet': '001',
    'urban_village': 'Dummy Urban Village',
    'sub_district': 'Dummy Sub-District',
    'same_hamlet_costs': '1000',
    'same_urban_village_different_hamlet_costs': '2000',
    'same_sub_district_different_urban_village_costs': '3000',
}

BATCH_DATA = {
    'batch_name': 'Batch 1',
    'start_date': '2020-12-26',
    'end_date': '2021-01-01',
    'shipping_cost': '60000',
}
