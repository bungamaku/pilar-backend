from django.utils.translation import gettext_lazy as _

DONATION_STATUS_CHOICES = [
    ('001', _('Waiting for admin confirmation')),
    ('002', _('Completed')),
    ('003', _('Canceled')),
    ('004', _('Waiting for reupload of proof of bank transfer')),
]

DONATION_TYPE_CHOICES = [
    ('CSH', _('Cash')),
    ('GDS', _('Goods')),
]

DONATION_GOODS_DELIVERY_METHOD_CHOICES = [
    ('PCK', _('Pick Up')),
    ('DLV', _('Delivered')),
]

PAYMENT_METHOD_CHOICES = [
    ('TRF', _('Transfer')),
    ('COD', _('Cash on delivery')),
]

TRANSACTION_STATUS_CHOICES = [
    ('001', _('Waiting for proof of payment')),
    ('002', _('Waiting for seller confirmation')),
    ('003', _('In process')),
    ('004', _('Being shipped')),
    ('005', _('Completed')),
    ('006', _('Canceled')),
    ('007', _('Failed')),
]
