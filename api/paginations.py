from rest_framework import pagination


class LargeResultsSetPagination(pagination.PageNumberPagination):
    max_page_size = 10000
    page_size = 1000
    page_size_query_param = 'page_size'


class SmallResultsSetPagination(pagination.PageNumberPagination):
    max_page_size = 100
    page_size = 10
    page_size_query_param = 'page_size'


class StandardResultsSetPagination(pagination.PageNumberPagination):
    max_page_size = 1000
    page_size = 100
    page_size_query_param = 'page_size'
