import decimal

from django.contrib.auth import password_validation
from django.core import exceptions
from django.utils.translation import gettext_lazy as _
from phonenumber_field import serializerfields
from rest_framework import serializers

from api import constants, models


class PhoneNumberSerializer(serializers.Serializer): # pylint: disable=abstract-method
    phone_number = serializerfields.PhoneNumberField(label=_('Phone number'))


class OTPSerializer(serializers.Serializer): # pylint: disable=abstract-method
    otp = serializers.CharField(label=_('OTP'), max_length=6)


class CartUpdateSerializer(serializers.Serializer): # pylint: disable=abstract-method
    product = serializers.UUIDField(label=_('Product'))
    quantity = serializers.IntegerField(label=_('Quantity'), min_value=0)
    hampers_messages = serializers.CharField(
        label=_('Hampers Messages'),
        max_length=100,
        required=False
    )


class CartCheckoutSerializer(serializers.Serializer): # pylint: disable=abstract-method
    payment_method = serializers.ChoiceField(
        choices=constants.PAYMENT_METHOD_CHOICES,
        label=_('Payment method')
    )
    donation = serializers.DecimalField(
        decimal_places=2,
        default=decimal.Decimal('0'),
        label=_('Donation'),
        max_digits=12,
        min_value=decimal.Decimal('0')
    )


class CartUploadPOPSerializer(serializers.Serializer): # pylint: disable=abstract-method
    transaction = serializers.UUIDField(label=_('Transaction'))
    proof_of_payment = serializers.ImageField(label=_('Proof of payment'))
    user_bank_name = serializers.CharField(
        label=_('User bank name'),
        max_length=100
    )
    user_bank_account_name = serializers.CharField(
        label=_('User bank account name'),
        max_length=200
    )
    bank_account_transfer_destination = serializers.UUIDField(
        label=_('Bank account transfer destination')
    )


class CartCompleteOrCancelTransactionSerializer(serializers.Serializer): # pylint: disable=abstract-method
    transaction = serializers.UUIDField(label=_('Transaction'))


class DonationCreateSerializer(serializers.Serializer): # pylint: disable=abstract-method
    program = serializers.UUIDField(label=_('Program'))
    donation_type = serializers.ChoiceField(
        choices=constants.DONATION_TYPE_CHOICES,
        label=_('Donation type')
    )
    amount = serializers.DecimalField(
        decimal_places=2,
        label=_('Amount'),
        max_digits=12,
        min_value=decimal.Decimal('0.01'),
        required=False
    )
    proof_of_bank_transfer = serializers.ImageField(
        label=_('Proof of bank transfer'),
        required=False
    )
    user_bank_name = serializers.CharField(
        label=_('User bank name'),
        max_length=100,
        required=False
    )
    user_bank_account_name = serializers.CharField(
        label=_('User bank account name'),
        max_length=200,
        required=False
    )
    bank_account_transfer_destination = serializers.UUIDField(
        label=_('Bank account transfer destination'),
        required=False
    )
    goods_quantity = serializers.DecimalField(
        decimal_places=0,
        label=_('Goods quantity'),
        max_digits=12,
        min_value=decimal.Decimal('1'),
        required=False
    )
    goods_description = serializers.CharField(
        label=_('Goods description'),
        max_length=200,
        required=False
    )
    delivery_method = serializers.ChoiceField(
        choices=constants.DONATION_GOODS_DELIVERY_METHOD_CHOICES,
        label=_('Goods delivery method'),
        required=False
    )
    delivery_address = serializers.CharField(
        label=_('Goods delivery address'),
        max_length=200,
        required=False
    )


class DonationReuploadProofOfBankTransferSerializer(serializers.Serializer): # pylint: disable=abstract-method
    program_donation = serializers.UUIDField(label=_('Program donasi'))
    amount = serializers.DecimalField(
        decimal_places=2,
        label=_('Amount'),
        max_digits=12,
        min_value=decimal.Decimal('0.01')
    )
    proof_of_bank_transfer = serializers.ImageField(label=_('Proof of bank transfer'))
    user_bank_name = serializers.CharField(
        label=_('User bank name'),
        max_length=100,
    )
    user_bank_account_name = serializers.CharField(
        label=_('User bank account name'),
        max_length=200
    )
    bank_account_transfer_destination = serializers.UUIDField(
        label=_('Bank account transfer destination')
    )


class ReportTransactionSerializer(serializers.Serializer): # pylint: disable=abstract-method
    created_at_date_range_after = serializers.DateField(
        label=_('Created from date'),
        required=False
    )
    created_at_date_range_before = serializers.DateField(
        label=_('Created to date'),
        required=False
    )


class ReportProgramDonationSerializer(serializers.Serializer): # pylint: disable=abstract-method
    created_at_date_range_after = serializers.DateField(
        label=_('Created from date'),
        required=False
    )
    created_at_date_range_before = serializers.DateField(
        label=_('Created to date'),
        required=False
    )


class UserSerializer(serializers.ModelSerializer):
    total_transactions = serializers.SerializerMethodField('get_total_transactions')
    total_program_donations_goods = serializers.SerializerMethodField('get_total_program_donations_goods')
    total_program_donations_cash = serializers.SerializerMethodField('get_total_program_donations_cash')

    class Meta:
        extra_kwargs = {'password': {'write_only': True}}
        fields = [
            'id',
            'username',
            'password',
            'full_name',
            'phone_number',
            'address',
            'neighborhood',
            'hamlet',
            'urban_village',
            'sub_district',
            'profile_picture',
            'total_transactions',
            'total_program_donations_goods',
            'total_program_donations_cash',
        ]
        model = models.User
        read_only_fields = ['id']

    def get_total_transactions(self, obj): # pylint: disable=no-self-use
        return obj.transactions.filter(transaction_status='005').count()

    def get_total_program_donations_goods(self, obj): # pylint: disable=no-self-use
        return obj.program_donations.filter(donation_type='GDS').filter(donation_status='002').count()
    
    def get_total_program_donations_cash(self, obj): # pylint: disable=no-self-use
        return obj.program_donations.filter(donation_type='CSH').filter(donation_status='002').count()

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

    def validate(self, attrs):
        user = models.User(**attrs)
        password = attrs.get('password')
        errors = {}
        if password is not None:
            try:
                password_validation.validate_password(password=password, user=user)
            except exceptions.ValidationError as err:
                errors['password'] = list(err.messages)
        if errors:
            raise serializers.ValidationError(errors)
        return super().validate(attrs)


class BankAccountTransferDestinationSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id', 'bank_name', 'bank_code_number', 'bank_account_number','bank_account_name']
        model = models.BankAccountTransferDestination
        read_only_fields = ['id']


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['id', 'name', 'image']
        model = models.Category
        read_only_fields = ['id']


class SubcategorySerializer(serializers.ModelSerializer):
    category_name = serializers.ReadOnlyField(source='category.name')

    class Meta:
        fields = ['id', 'name', 'category', 'category_name', 'image']
        model = models.Subcategory
        read_only_fields = ['id']


class ProductSerializer(serializers.ModelSerializer):
    category = serializers.ReadOnlyField(source='subcategory.category.pk')
    category_name = serializers.ReadOnlyField(source='subcategory.category.name')
    subcategory_name = serializers.ReadOnlyField(source='subcategory.name')

    class Meta:
        fields = [
            'id',
            'code',
            'name',
            'category',
            'category_name',
            'subcategory',
            'subcategory_name',
            'description',
            'price',
            'stock',
            'modal',
            'profit',
            'image',
            'total_profit',
            'unit',
            'is_hampers',
            'hampers_price',
            'preorder',
            'preorder_duration'
        ]
        model = models.Product
        read_only_fields = ['id', 'code']

    


class CartItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)

    class Meta:
        fields = ['id', 'product', 'quantity', 'hampers_messages']
        model = models.CartItem
        read_only_fields = ['id', 'quantity']


class ShoppingCartSerializer(serializers.ModelSerializer):
    user_username = serializers.ReadOnlyField(source='user.username')
    cart_items = CartItemSerializer(many=True, read_only=True)
    cart_item_subtotal = serializers.SerializerMethodField('get_cart_item_subtotal')

    class Meta:
        fields = ['id', 'user', 'user_username', 'cart_items', 'cart_item_subtotal']
        model = models.ShoppingCart
        read_only_fields = ['id', 'user']

    def get_cart_item_subtotal(self, obj): # pylint: disable=no-self-use
        cart_item_subtotal = sum(
            (cart_item.product.price + cart_item.product.hampers_price) 
            * cart_item.quantity for cart_item in obj.cart_items.all()
        )
        return str(cart_item_subtotal)


class TransactionItemSerializer(serializers.ModelSerializer):
    product_code = serializers.ReadOnlyField(source='product.code')

    class Meta:
        fields = [
            'id',
            'product',
            'product_code',
            'product_name',
            'product_price',
            'hampers_price',
            'hampers_messages',
            'quantity',
        ]
        model = models.TransactionItem
        read_only_fields = [
            'id',
            'product',
            'product_name',
            'product_price',
            'hampers_price',
            'quantity',
        ]


class TransactionSerializer(serializers.ModelSerializer):
    user_username = serializers.ReadOnlyField(source='user.username')
    readable_payment_method = serializers.SerializerMethodField('get_readable_payment_method')
    readable_transaction_status = serializers.SerializerMethodField(
        'get_readable_transaction_status'
    )
    transaction_items = TransactionItemSerializer(many=True, read_only=True)
    transaction_item_subtotal = serializers.SerializerMethodField('get_transaction_item_subtotal')
    subtotal = serializers.SerializerMethodField('get_subtotal')

    class Meta:
        fields = [
            'id',
            'transaction_number',
            'user',
            'user_username',
            'user_full_name',
            'user_phone_number',
            'shipping_address',
            'shipping_neighborhood',
            'shipping_hamlet',
            'shipping_urban_village',
            'shipping_sub_district',
            'shipping_costs',
            'payment_method',
            'readable_payment_method',
            'donation',
            'transaction_status',
            'readable_transaction_status',
            'proof_of_payment',
            'user_bank_name',
            'user_bank_account_name',
            'bank_account_transfer_destination',
            'transfer_destination_bank_name',
            'transfer_destination_bank_account_name',
            'transfer_destination_bank_code_number',
            'transfer_destination_bank_account_number',
            'created_at',
            'updated_at',
            'transaction_items',
            'transaction_item_subtotal',
            'subtotal',
            'batch',
            'batch_name',
            'end_date'
        ]
        model = models.Transaction
        read_only_fields = [
            'id',
            'transaction_number',
            'user',
            'user_full_name',
            'user_phone_number',
            'shipping_address',
            'shipping_neighborhood',
            'shipping_hamlet',
            'shipping_urban_village',
            'shipping_sub_district',
            'shipping_costs',
            'payment_method',
            'donation',
            'proof_of_payment',
            'user_bank_name',
            'user_bank_account_name',
            'bank_account_transfer_destination',
            'transfer_destination_bank_name',
            'transfer_destination_bank_account_name',
            'transfer_destination_bank_code_number',
            'transfer_destination_bank_account_number',
            'created_at',
            'updated_at',
            'batch',
            'batch_name',
            'end_date'
        ]

    def get_readable_payment_method(self, obj): # pylint: disable=no-self-use
        return obj.get_payment_method_display()

    def get_readable_transaction_status(self, obj): # pylint: disable=no-self-use
        return obj.get_transaction_status_display()

    def get_transaction_item_subtotal(self, obj): # pylint: disable=no-self-use
        transaction_item_subtotal = sum(
            (transaction_item.product_price + transaction_item.hampers_price)
            * transaction_item.quantity for transaction_item in obj.transaction_items.all()
        )
        return str(transaction_item_subtotal)

    def get_subtotal(self, obj):
        subtotal = (decimal.Decimal(self.get_transaction_item_subtotal(obj)) + obj.shipping_costs +
                    obj.donation)
        return str(subtotal)

    def validate(self, attrs):
        transaction_status = attrs['transaction_status']
        errors = {}
        if transaction_status == '007':
            errors['transaction_status'] = _('Cannot update transaction status to failed.')
        if errors:
            raise serializers.ValidationError(errors)
        return super().validate(attrs)


class ProgramSerializer(serializers.ModelSerializer):
    total_donation_amount = serializers.SerializerMethodField('get_total_donation_amount')
    goods_donation = serializers.SerializerMethodField('get_list_goods_donation')

    class Meta:
        fields = [
            'id',
            'code',
            'name',
            'description',
            'start_date_time',
            'end_date_time',
            'location',
            'speaker',
            'open_donation',
            'program_minutes',
            'poster_image',
            'link',
            'total_donation_amount',
            'goods_donation',
        ]
        model = models.Program
        read_only_fields = ['id', 'code']

    def get_total_donation_amount(self, obj): # pylint: disable=no-self-use
        total_donation_amount = sum(
            program_donation.amount for program_donation in obj.program_donations.filter(
                donation_status='002',
                donation_type='CSH'
            )
        )
        return str(total_donation_amount)
    def get_list_goods_donation(self, obj):
        lst = obj.program_donations.filter(
                donation_status='002',
                donation_type='GDS'
            )
        lst_ret = []
        for goods_donation in lst:
            dct = {'id' : goods_donation.id, 'desc' : goods_donation.goods_description, 'quantity' : goods_donation.goods_quantity}
            lst_ret.append(dct)
        return lst_ret

    def validate(self, attrs):
        instance = self.instance
        start_date_time = attrs.get('start_date_time', getattr(instance, 'start_date_time', None))
        end_date_time = attrs.get('end_date_time', getattr(instance, 'end_date_time', None))
        errors = {}
        if ((start_date_time is not None) and (end_date_time is not None) and
                (end_date_time <= start_date_time)):
            errors['end_date_time'] = _('End date time should be greater than start date time.')
        if errors:
            raise serializers.ValidationError(errors)
        return super().validate(attrs)


class ProgramProgressSerializer(serializers.ModelSerializer):

    class Meta:
        fields = [
            'id',
            'program',
            'date',
            'description',
            'image',
        ]
        model = models.ProgramProgress
        read_only_fields = [
            'id',
            'program'
        ]

    def validate(self, attrs):
        instance = self.instance
        errors = {}
        if errors:
            raise serializers.ValidationError(errors)
        return super().validate(attrs)


class ProgramDonationSerializer(serializers.ModelSerializer):
    program_code = serializers.ReadOnlyField(source='program.code')
    user_username = serializers.ReadOnlyField(source='user.username')
    readable_donation_status = serializers.SerializerMethodField(
        'get_readable_donation_status'
    )

    class Meta:
        fields = [
            'id',
            'donation_number',
            'program',
            'program_code',
            'user',
            'user_username',
            'user_full_name',
            'user_phone_number',
            'program_name',
            'donation_type',
            'donation_status',
            'amount',
            'readable_donation_status',
            'proof_of_bank_transfer',
            'user_bank_name',
            'user_bank_account_name',
            'bank_account_transfer_destination',
            'transfer_destination_bank_name',
            'transfer_destination_bank_account_name',
            'transfer_destination_bank_code_number',
            'transfer_destination_bank_account_number',
            'goods_quantity',
            'goods_description',
            'delivery_method',
            'delivery_address',
            'created_at',
            'updated_at',
        ]
        model = models.ProgramDonation
        read_only_fields = [
            'id',
            'donation_number',
            'program',
            'user',
            'user_full_name',
            'user_phone_number',
            'program_name',
            'donation_type',
            'amount',
            'proof_of_bank_transfer',
            'user_bank_name',
            'user_bank_account_name',
            'bank_account_transfer_destination',
            'transfer_destination_bank_name',
            'transfer_destination_bank_account_name',
            'transfer_destination_bank_code_number',
            'transfer_destination_bank_account_number',
            'created_at',
            'updated_at',
        ]

    def get_readable_donation_status(self, obj): # pylint: disable=no-self-use
        return obj.get_donation_status_display()


class AppConfigSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['send_sms']
        model = models.AppConfig


class HelpContactConfigSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['email']
        model = models.HelpContactConfig


class ShipmentConfigSerializer(serializers.ModelSerializer):
    class Meta:
        fields = [
            'hamlet',
            'urban_village',
            'sub_district',
            'same_hamlet_costs',
            'same_urban_village_different_hamlet_costs',
            'same_sub_district_different_urban_village_costs',
        ]
        model = models.ShipmentConfig

class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        fields = [
            'id',
            'batch_name',
            'start_date',
            'end_date',
            'shipping_cost'
        ]
        model = models.Batch

class BatchCreateSerializer(serializers.Serializer): # pylint: disable=abstract-method
    batch_name = serializers.CharField(
        label=_('Batch name'),
        max_length=200
    )
    start_date = serializers.DateField(label=_('Start Date'))
    end_date = serializers.DateField(label=_('End Date'))
    shipping_cost = serializers.DecimalField(
        decimal_places=2,
        label=_('Amount'),
        max_digits=12,
        min_value=decimal.Decimal('0.01')
    )
